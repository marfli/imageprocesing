﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Emgu;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.UI;
using System.Threading;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;
using System.Windows.Forms.DataVisualization.Charting;

namespace ImageProcessingMgr
{
    public partial class Form1 : Form
    {
        Point lastPoint = Point.Empty;//Point.Empty represents null for a Point object

        bool isMouseDown = new Boolean();//this is used to evaluate whether our mousebutton is down or not


        Bitmap pBoxImage;
        Bitmap pBoxImage2;
        Bitmap pBoxImage1;

        Point firstPoint = new Point(-1, -1);
        Point secondPoint = new Point(-1, -1);

        Image<Bgr, byte> inputImage;
        Image<Bgr, byte> inputImage2;
        Image<Bgr, byte> inputImage1;
        Image<Gray, byte> grayImage;

        DigitalStructure digitalstructure;
        List<Grain> grains;
        Random rand = new Random();
        Thread thread;
        HashSet<Cell> getAllRedPoints = new HashSet<Cell>();



        DigitalStructure digitalstructure2;

        bool isProcessing = false;
        bool wasProcessed = false;
        int width;
        int height;



        List<Color> BoundaryList = new List<Color>();
        List<double> CoordinatesList = new List<double>();
        DenseHistogram histogram = new DenseHistogram(256, new RangeF(0, 255));
        Cell[,] prevCells, currentCells;
        bool colorFlag = true;



        public Form1()
        {
            InitializeComponent();


        }


        Bitmap _currentImage;
        public Bitmap currentImage
        {
            get
            {
                return _currentImage;
            }
            set
            {
                pictureBox1.Image = _currentImage = value;
                pictureBox1.Width = _currentImage.Width;
                pictureBox1.Height = _currentImage.Height;
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }
        Bitmap _currentImage2;
        public Bitmap currentImage2
        {
            get
            {
                return _currentImage2;
            }
            set
            {
                pictureBox2.Image = _currentImage2 = value;
                pictureBox2.Width = _currentImage2.Width;
                pictureBox2.Height = _currentImage2.Height;
            }
        }

        //zmienił bym nazwe na point
        // a ja nie !!!!
        public class Cell
        {
            int x, y = 0;
            Color color;
            bool ischecked = false;


            public Cell()
            {
                this.x = 0;
                this.y = 0;
                this.color = Color.FromArgb(255, 255, 255);
                this.ischecked = false;
            }

            public Cell(int x, int y)
            {
                this.x = x;
                this.y = y;
                this.color = Color.FromArgb(255, 255, 255);
                this.ischecked = false;
            }

            public Cell(int x, int y, Color color)
            {
                this.x = x;
                this.y = y;
                this.color = color;
                this.ischecked = false;
            }

            public Cell(Color color)
            {
                this.color = color;
            }

            public Color Color { get => color; set => color = value; }
            public int X { get => x; set => x = value; }
            public int Y { get => y; set => y = value; }
            public bool Ischecked { get => ischecked; set => ischecked = value; }
        }
        class Grain
        {
            public List<Cell> points;
            public List<Cell> edges;
            public int Id;
            public Color color;
            public double percent;
            public double surfaceArea;
            public double diameter;
            public double rangeGrainValue;

            public Grain(int pointsCount)
            {
                points = new List<Cell>();
                edges = new List<Cell>();
            }



        }

        public class Range
        {
            public double idRange;

            public double min;
            public double max;



            public Range(double start, double end)
            {

            }

        }

        public class DualPhaseCells
        {
            public double percentage;
            public List<Cell> blackCells;

            public DualPhaseCells(int countBlackPoints)
            {
                blackCells = new List<Cell>();
            }
        }
        //odowiada za całą strukture (cały obraz)
        class DigitalStructure
        {
            public int width;
            public int height;
            public Cell[,] digitalImage;
            public List<Grain> grains;
            public List<DualPhaseCells> blackCellsDual;
            public DigitalStructure(int widthh, int heightt)
            {
                width = widthh;
                height = heightt;
                grains = new List<Grain>();
                digitalImage = new Cell[width, height];
                blackCellsDual = new List<DualPhaseCells>();

                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        digitalImage[i, j] = new Cell(i, j);

                    }
                }
            }
        }



        class Structure2
        {
            Cell[,] digitalImage;

            private int width;
            private int height;

            public Structure2(int width, int height)
            {
                this.width = width;
                this.height = height;

                digitalImage = new Cell[width, height];

                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        digitalImage[i, j] = new Cell(i, j);

                    }
                }
            }
            public Cell[,] DigitalImage { get => digitalImage; set => digitalImage = value; }

            public int StructHeight { get => height; set => height = value; }
            public int StructWidth { get => width; set => width = value; }

        }


        private void openBMPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog img = new OpenFileDialog();
                if (img.ShowDialog() == DialogResult.OK)
                {
                    inputImage = new Image<Bgr, byte>(img.FileName);



                    inputImage = new Image<Bgr, byte>(img.FileName);
                    width = inputImage.Width;
                    height = inputImage.Height;
                    currentImage = inputImage.Bitmap;


                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void trackBar1_vertical_Scroll(object sender, EventArgs e)
        {
            width = (inputImage.Width * trackBar1_vertical.Value) / 100;
            currentImage = ResieNow(width, height);
        }

        private void trackBar1_horizontal_Scroll(object sender, EventArgs e)
        {

            height = (inputImage.Height * trackBar1_horizontal.Value) / 100;
            currentImage = ResieNow(width, height);
        }


        private Bitmap ResieNow(int inputImage_width, int inputImage_height)
        {
            Rectangle dest_rect = new Rectangle(0, 0, inputImage_width, inputImage_height);
            Bitmap destImage = new Bitmap(inputImage_width, inputImage_height);
            destImage.SetResolution(inputImage.Bitmap.HorizontalResolution, inputImage.Bitmap.VerticalResolution);
            using (var g = Graphics.FromImage(destImage))
            {
                g.CompositingMode = CompositingMode.SourceCopy;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    g.DrawImage(inputImage.ToBitmap(), dest_rect, 0, 0, inputImage.Width, inputImage.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            return destImage;
        }

        private void applyImageSize_Click(object sender, EventArgs e)
        {
            //Bitmap imageTmp = new Bitmap(width, height);
            //digitalstructure = new DigitalStructure(inputImage.Size.Width, inputImage.Size.Height);

            //for (int i = 0; i < width; i++)
            //{
            //    for (int j = 0; j < height; j++)
            //    {
            //        digitalstructure.digitalImage[i, j] = new Cell(i, j, currentImage.GetPixel(i, j));
            //        imageTmp.SetPixel(i, j, digitalstructure.digitalImage[i, j].Color);

            //        if (i == 0 || i == currentImage.Width - 1 || j == 0 || j == currentImage.Height - 1)
            //        {
            //            digitalstructure.digitalImage[i, j].Color = Color.FromArgb(0, 0, 0);
            //            imageTmp.SetPixel(i, j, Color.FromArgb(0, 0, 0));
            //        }

            //    }
            //}

            //for (int i = 0; i < inputImage.Size.Width; i++)
            //{
            //    for (int j = 0; j < inputImage.Size.Height; j++)
            //    {
            //        digitalstructure.digitalImage[i, j] = new Cell(i, j, inputImage.Bitmap.GetPixel(i, j));
            //        imageTmp.SetPixel(i, j, digitalstructure.digitalImage[i, j].Color);

            //        if (i == 0 || i == inputImage.Size.Width - 1 || j == 0 || j == inputImage.Size.Height - 1)
            //        {
            //            digitalstructure.digitalImage[i, j].Color = Color.FromArgb(0, 0, 0);
            //            imageTmp.SetPixel(i, j, Color.FromArgb(0, 0, 0));
            //        }

            //    }
            //}


            Bitmap imageTmp = new Bitmap(currentImage.Width, currentImage.Height);
            digitalstructure = new DigitalStructure(currentImage.Width, currentImage.Height);


            for (int i = 0; i < currentImage.Width; i++)
            {
                for (int j = 0; j < currentImage.Height; j++)
                {
                    digitalstructure.digitalImage[i, j] = new Cell(i, j, currentImage.GetPixel(i, j));
                    imageTmp.SetPixel(i, j, digitalstructure.digitalImage[i, j].Color);

                    if (i == 0 || i == currentImage.Width - 1 || j == 0 || j == currentImage.Height - 1)
                    {
                        digitalstructure.digitalImage[i, j].Color = Color.FromArgb(0, 0, 0);
                        imageTmp.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                    }

                }
            }

            digitalstructure2 = digitalstructure;
            currentImage = currentImage2 = imageTmp;



            digitalstructure2 = digitalstructure;
            currentImage = currentImage2 = imageTmp;
            currentImage2 = imageTmp;
            pictureBox2.Image = currentImage2;


            tabControl1.Visible = true;
            trackBar1_horizontal.Visible = false;
            trackBar1_vertical.Visible = false;
            applyImageSize.Visible = false;    
            pictureBox2.Visible = true;
            radioButton4.Visible = true;
    
        }

        private void Erosion()
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(pictureBox1.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);
                grayImage = inputImage.Convert<Gray, Byte>();

                int iteration = (int)numeric.Value;
                inputImage._Erode(iteration);

                pictureBox1.Image = DigitalStuctureUpdate(inputImage.ToBitmap());
            }

            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);
                grayImage = inputImage.Convert<Gray, Byte>();

                int iteration = (int)numeric.Value;
                inputImage._Erode(iteration);

                pictureBox2.Image = DigitalStuctureUpdate(inputImage.ToBitmap());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
            Dilation();
        }


        private void Dilation()
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                try
                {
                    pBoxImage = new Bitmap(pictureBox1.Image);
                    inputImage = new Image<Bgr, Byte>(pBoxImage);
                    grayImage = inputImage.Convert<Gray, Byte>();

                    int iteration = (int)numeric.Value;
                    inputImage._Dilate(iteration);

                    pictureBox1.Image = DigitalStuctureUpdate(inputImage.ToBitmap());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }
            else if (radioButton4.Checked)
            {
                try
                {
                    pBoxImage = new Bitmap(pictureBox2.Image);
                    inputImage = new Image<Bgr, Byte>(pBoxImage);
                    grayImage = inputImage.Convert<Gray, Byte>();
                    int iteration = (int)numeric.Value;
                    inputImage._Dilate(iteration);
                    pictureBox2.Image = DigitalStuctureUpdate(inputImage.ToBitmap());

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Erosion();

        }

        //wrong update method not inputImage
        private Bitmap DigitalStuctureUpdate(Bitmap bitmapNo)
        {
            Bitmap imageTmp = bitmapNo;
            if (radioButton3.Checked)
            {
                for (int i = 0; i < digitalstructure.width; i++)
                {
                    for (int j = 0; j < digitalstructure.height; j++)
                    {
                        digitalstructure.digitalImage[i, j].Color = bitmapNo.GetPixel(i, j);

                        if (i == 0 || i == inputImage.Size.Width - 1 || j == 0 || j == inputImage.Size.Height - 1)
                        {
                            digitalstructure.digitalImage[i, j].Color = Color.FromArgb(0, 0, 0);
                            imageTmp.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                        }

                    }
                }
            }
            else if (radioButton4.Checked)
            {
                for (int i = 0; i < digitalstructure2.width; i++)
                {
                    for (int j = 0; j < digitalstructure2.height; j++)
                    {
                        digitalstructure2.digitalImage[i, j].Color = bitmapNo.GetPixel(i, j);

                        if (i == 0 || i == inputImage.Size.Width - 1 || j == 0 || j == inputImage.Size.Height - 1)
                        {
                            digitalstructure2.digitalImage[i, j].Color = Color.FromArgb(0, 0, 0);
                            imageTmp.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                        }

                    }
                }
            }

            return imageTmp;
        }


        private void setRandomColor(Cell cell)
        {
            int r, g, b, wsp;
            do
            {
                wsp = rand.Next(1000);
                r = rand.Next(0, (256 + wsp) + 1) % 256;
                g = rand.Next(0, (512 + wsp) + 1) % 256;
                b = rand.Next(0, (1024 + wsp) + 1) % 256;
            } while (Color.FromArgb(r, g, b).Equals(Color.FromArgb(255, 255, 255)) || Color.FromArgb(r, g, b).Equals(Color.FromArgb(0, 0, 0)));

            cell.Color = Color.FromArgb(r, g, b);
        }

        Color getMaxKey(Dictionary<Color, int> map)
        {
            int max = 0;

            foreach (var item in map)  // item - kolor i ilosc jego wystapien
            {
                if (item.Value > max)  //  szuka koloru, ktory wystapil najwiecej razy (item.Value - ilość wystąpień)
                    max = item.Value;
            }
            Color maxKey = map.FirstOrDefault(x => x.Value == max).Key;  //zwraca kolor(klucz), który wystąpił najwięcej razy

            return maxKey;
        }

        private void bMPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                int width = Convert.ToInt32(pictureBox1.Width);
                int height = Convert.ToInt32(pictureBox1.Height);
                Bitmap bmp = new Bitmap(width, height);
                pictureBox1.DrawToBitmap(bmp, new Rectangle(0, 0, width, height));
                bmp.Save(dialog.FileName, ImageFormat.Bmp);
            }
        }

        private void cleanSpaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = null;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {

            grainBoundaries();
        }

        private void grainBoundaries()
        {
            int gbSize = (int)gbSizeUpDown.Value;
            int wUpDown = digitalstructure.width;
            int hUpDown = digitalstructure.height;
            int iprev, inext, jprev, jnext = 0;


            Bitmap imageTmp2 = new Bitmap(digitalstructure.width, digitalstructure.height);

            wasProcessed = true;
            for (int i = 0; i < digitalstructure.width; i++)
            {
                for (int j = 0; j < digitalstructure.height; j++)
                {
                    if (i == 0)
                        iprev = wUpDown - 1;
                    else
                        iprev = i - 1;

                    if (i == wUpDown - 1)
                        inext = 0;
                    else
                        inext = i + 1;

                    if (j == 0)
                        jprev = hUpDown - 1;
                    else
                        jprev = j - 1;

                    if (j == hUpDown - 1)
                        jnext = 0;
                    else
                        jnext = j + 1;

                    int coordinateXPlusGbSize = i + gbSize;
                    int coordinateYPlusGbSize = j + gbSize;

                    Color color = digitalstructure.digitalImage[i, j].Color;

                    if (BoundaryList.Count == 0)
                    {
                        if (wasProcessed && color != Color.FromArgb(0, 0, 0))
                        {
                            if (digitalstructure.digitalImage[iprev, jprev].Color != color && digitalstructure.digitalImage[iprev, jprev].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[iprev, j].Color != color && digitalstructure.digitalImage[iprev, j].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[iprev, jnext].Color != color && digitalstructure.digitalImage[iprev, jnext].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[i, jnext].Color != color && digitalstructure.digitalImage[i, jnext].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[inext, jnext].Color != color && digitalstructure.digitalImage[inext, jnext].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[inext, j].Color != color && digitalstructure.digitalImage[inext, j].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[inext, jprev].Color != color && digitalstructure.digitalImage[inext, jprev].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[i, jprev].Color != color && digitalstructure.digitalImage[i, jprev].Color != Color.FromArgb(0, 0, 0))
                            {
                                if (coordinateXPlusGbSize < wUpDown && coordinateYPlusGbSize < hUpDown)
                                    for (int k = i; k < coordinateXPlusGbSize; k++)
                                    {
                                        for (int l = j; l < coordinateYPlusGbSize; l++)
                                        {
                                            digitalstructure.digitalImage[k, l].Color = Color.FromArgb(0, 0, 0);
                                        }
                                    }
                            }
                        }
                    }
                    else
                    {
                        if (wasProcessed && color != Color.FromArgb(0, 0, 0) && BoundaryList.Contains(color))
                        {
                            if (digitalstructure.digitalImage[iprev, jprev].Color != color && digitalstructure.digitalImage[iprev, jprev].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[iprev, j].Color != color && digitalstructure.digitalImage[iprev, j].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[iprev, jnext].Color != color && digitalstructure.digitalImage[iprev, jnext].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[i, jnext].Color != color && digitalstructure.digitalImage[i, jnext].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[inext, jnext].Color != color && digitalstructure.digitalImage[inext, jnext].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[inext, j].Color != color && digitalstructure.digitalImage[inext, j].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[inext, jprev].Color != color && digitalstructure.digitalImage[inext, jprev].Color != Color.FromArgb(0, 0, 0) ||
                                digitalstructure.digitalImage[i, jprev].Color != color && digitalstructure.digitalImage[i, jprev].Color != Color.FromArgb(0, 0, 0))
                            {
                                if (coordinateXPlusGbSize < wUpDown && coordinateYPlusGbSize < hUpDown)
                                    for (int k = i; k < coordinateXPlusGbSize; k++)
                                    {
                                        for (int l = j; l < coordinateYPlusGbSize; l++)
                                        {
                                            digitalstructure.digitalImage[k, l].Color = Color.FromArgb(0, 0, 0);
                                        }
                                    }
                            }
                        }
                    }
                    imageTmp2.SetPixel(i, j, digitalstructure.digitalImage[i, j].Color);
                }
            }

            pictureBox1.Image = currentImage2 = imageTmp2;
            // updatePrevCells();
            // refreshStructureBox();
        }



        private void button6_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(pictureBox1.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);
                colorFlag = true;
                Image<Bgr, byte> resizedImages = inputImage.Rotate(90, new Bgr(), false);
                inputImage = resizedImages;
                colorFlag = true;

                if (colorFlag)
                {
                    pictureBox1.Image = inputImage.ToBitmap();
                }

                pictureBox1.Refresh();
            }
            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);
                colorFlag = true;
                Image<Bgr, byte> resizedImages = inputImage.Rotate(90, new Bgr(), false);
                inputImage = resizedImages;
                colorFlag = true;

                if (colorFlag)
                {
                    pictureBox2.Image = inputImage.ToBitmap();
                }

                pictureBox2.Refresh();
            }


        }

        private double currentBrightness;
        private void brightnessTrackBar_Scroll(object sender, EventArgs e)
        {

            brightnessValue.Text = brightnessTrackBar.Value.ToString();
            currentBrightness = brightnessTrackBar.Value;
            ApplyAllTransforms();
        }

        private double currentContrast;
        private void contrastTrackBar_Scroll(object sender, EventArgs e)
        {

            contrastValue.Text = contrastTrackBar.Value.ToString();
            currentContrast = contrastTrackBar.Value;

            ApplyAllTransforms();
        }

        private double currentSharpen;
        private void sharpenTrackBar_Scroll(object sender, EventArgs e)
        {
            sharpenValue.Text = sharpenTrackBar.Value.ToString();
            currentSharpen = contrastTrackBar.Value;
            ApplyAllTransforms();
        }

        public static Bitmap ApplyBrightness(Bitmap Image, int Value)
        {
            Bitmap TempBitmap = Image;
            float FinalValue = (float)Value / 255.0f;
            Bitmap NewBitmap = new Bitmap(TempBitmap.Width, TempBitmap.Height);
            Graphics NewGraphics = Graphics.FromImage(NewBitmap);
            float[][] FloatColorMatrix =
            {
                new float [] {1,0,0,0,0},
                new float [] {0,1,0,0,0},
                new float [] {0,0,1,0,0},
                new float [] {0,0,0,1,0},
                new float [] {FinalValue, FinalValue, FinalValue, 1, 1}
            };

            ColorMatrix NewColorMatrix = new ColorMatrix(FloatColorMatrix);
            ImageAttributes Attributes = new ImageAttributes();
            Attributes.SetColorMatrix(NewColorMatrix);
            NewGraphics.DrawImage(TempBitmap, new Rectangle(0, 0, TempBitmap.Width, TempBitmap.Height), 0, 0, TempBitmap.Width, TempBitmap.Height, GraphicsUnit.Pixel, Attributes);
            Attributes.Dispose();
            NewGraphics.Dispose();
            return NewBitmap;

        }
        public static Bitmap ApplyContrast(Bitmap Image, int Value)
        {
            Bitmap TempBitmap = Image;
            float FinalValue = (float)Value * 0.04f;
            Bitmap NewBitmap = new Bitmap(TempBitmap.Width, TempBitmap.Height);
            Graphics NewGraphics = Graphics.FromImage(NewBitmap);
            float[][] FloatColorMatrix =
            {
                new float [] { FinalValue, 0f, 0f, 0f, 0f},
                new float [] { 0f, FinalValue, 0f, 0f, 0f},
                new float [] { 0f, 0f, FinalValue, 0f, 0f},
                new float [] { 0f, 0f, 0f, 1f, 0f},
                new float [] {0.001f, 0.001f, 0.001f, 0f, 1f}
            };

            ColorMatrix NewColorMatrix = new ColorMatrix(FloatColorMatrix);
            ImageAttributes Attributes = new ImageAttributes();
            Attributes.SetColorMatrix(NewColorMatrix);
            NewGraphics.DrawImage(TempBitmap, new Rectangle(0, 0, TempBitmap.Width, TempBitmap.Height), 0, 0, TempBitmap.Width, TempBitmap.Height, GraphicsUnit.Pixel, Attributes);
            Attributes.Dispose();
            NewGraphics.Dispose();
            return NewBitmap;
        }

        public static Bitmap Sharpen(Bitmap Image, double strength)
        {
            using (var bitmap = Image as Bitmap)
            {
                if (bitmap != null)
                {
                    var sharpenImage = bitmap.Clone() as Bitmap;

                    int width = Image.Width;
                    int height = Image.Height;

                    // Create sharpening filter.
                    const int filterWidth = 5;
                    const int filterHeight = 5;

                    var filter = new double[,]
                        {
                    {-1, -1, -1, -1, -1},
                    {-1,  2,  2,  2, -1},
                    {-1,  2, 16,  2, -1},
                    {-1,  2,  2,  2, -1},
                    {-1, -1, -1, -1, -1}
                        };

                    double bias = 1.0 - strength;
                    double factor = strength / 16.0;

                    var result = new Color[Image.Width, Image.Height];

                    // Lock image bits for read/write.
                    if (sharpenImage != null)
                    {
                        BitmapData pbits = sharpenImage.LockBits(new Rectangle(0, 0, width, height),
                                                                    ImageLockMode.ReadWrite,
                                                                    PixelFormat.Format24bppRgb);

                        // Declare an array to hold the bytes of the bitmap.
                        int bytes = pbits.Stride * height;
                        var rgbValues = new byte[bytes];

                        // Copy the RGB values into the array.
                        Marshal.Copy(pbits.Scan0, rgbValues, 0, bytes);

                        int rgb;
                        // Fill the color array with the new sharpened color values.
                        for (int x = 0; x < width; ++x)
                        {
                            for (int y = 0; y < height; ++y)
                            {
                                double red = 0.0, green = 0.0, blue = 0.0;

                                for (int filterX = 0; filterX < filterWidth; filterX++)
                                {
                                    for (int filterY = 0; filterY < filterHeight; filterY++)
                                    {
                                        int imageX = (x - filterWidth / 2 + filterX + width) % width;
                                        int imageY = (y - filterHeight / 2 + filterY + height) % height;

                                        rgb = imageY * pbits.Stride + 3 * imageX;

                                        red += rgbValues[rgb + 2] * filter[filterX, filterY];
                                        green += rgbValues[rgb + 1] * filter[filterX, filterY];
                                        blue += rgbValues[rgb + 0] * filter[filterX, filterY];
                                    }

                                    rgb = y * pbits.Stride + 3 * x;

                                    int r = Math.Min(Math.Max((int)(factor * red + (bias * rgbValues[rgb + 2])), 0), 255);
                                    int g = Math.Min(Math.Max((int)(factor * green + (bias * rgbValues[rgb + 1])), 0), 255);
                                    int b = Math.Min(Math.Max((int)(factor * blue + (bias * rgbValues[rgb + 0])), 0), 255);

                                    result[x, y] = Color.FromArgb(r, g, b);
                                }
                            }
                        }

                        // Update the image with the sharpened pixels.
                        for (int x = 0; x < width; ++x)
                        {
                            for (int y = 0; y < height; ++y)
                            {
                                rgb = y * pbits.Stride + 3 * x;

                                rgbValues[rgb + 2] = result[x, y].R;
                                rgbValues[rgb + 1] = result[x, y].G;
                                rgbValues[rgb + 0] = result[x, y].B;
                            }
                        }

                        // Copy the RGB values back to the bitmap.
                        Marshal.Copy(rgbValues, 0, pbits.Scan0, bytes);
                        // Release image bits.
                        sharpenImage.UnlockBits(pbits);
                    }

                    return sharpenImage;
                }
            }
            return null;
        }

        private void ApplyAllTransforms()
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                Bitmap picture = this.currentImage;

                picture = ApplyBrightness(picture, brightnessTrackBar.Value);
                picture = ApplyContrast(picture, contrastTrackBar.Value);
                picture = Sharpen(picture, sharpenTrackBar.Value);


                pictureBox1.Image = picture;
            }
            else if (radioButton4.Checked)
            {
                Bitmap picture = this.currentImage2;

                picture = ApplyBrightness(picture, brightnessTrackBar.Value);
                picture = ApplyContrast(picture, contrastTrackBar.Value);
                picture = Sharpen(picture, sharpenTrackBar.Value);

                pictureBox2.Image = picture;
            }

        }

        private void resetSlidersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            brightnessTrackBar.Value = brightnessTrackBar.Minimum;
            contrastTrackBar.Value = contrastTrackBar.Minimum;
            sharpenTrackBar.Value = sharpenTrackBar.Minimum;

        }

        private void flipHorizontally_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                colorFlag = true;

                pBoxImage = new Bitmap(currentImage);
                inputImage1 = new Image<Bgr, Byte>(pBoxImage);
                inputImage1._Flip(FlipType.Horizontal);

                if (colorFlag)
                {
                    currentImage = inputImage1.ToBitmap();
                }
            }
            else if (radioButton4.Checked)
            {
                colorFlag = true;
                pBoxImage = new Bitmap(currentImage2);
                inputImage1 = new Image<Bgr, Byte>(pBoxImage);

                inputImage1._Flip(FlipType.Horizontal);

                if (colorFlag)
                {
                    currentImage2 = inputImage1.ToBitmap();
                    //pictureBox2.Image = diagitalImage.ToBitmap();
                }

                //pictureBox2.Refresh();
                //metoda do aktualizacji digitalImage
            }

        }

        private void flipVerticaly_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                colorFlag = true;
                pBoxImage = new Bitmap(currentImage);
                inputImage1 = new Image<Bgr, Byte>(pBoxImage);

                inputImage1._Flip(FlipType.Vertical);

                if (colorFlag)
                {
                    currentImage = inputImage1.ToBitmap();
                }


            }
            else if (radioButton4.Checked)
            {
                colorFlag = true;
                pBoxImage = new Bitmap(currentImage2);
                inputImage1 = new Image<Bgr, Byte>(pBoxImage);

                inputImage1._Flip(FlipType.Vertical);

                if (colorFlag)
                {
                    currentImage2 = inputImage1.ToBitmap();
                }


            }

        }

        public void ResizeImage()
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                int widthImage = (int)widthSize.Value;
                int heightImage = (int)heightSize.Value;
                pBoxImage = new Bitmap(pictureBox1.Image);
                inputImage1 = new Image<Bgr, Byte>(pBoxImage);
                colorFlag = true;

                Image<Bgr, byte> resizedImage = inputImage1.Resize(widthImage, heightImage, Inter.Linear);
                inputImage1 = resizedImage;
                if (colorFlag)
                {
                    currentImage = inputImage1.ToBitmap();
                }
            }


            else if (radioButton4.Checked)
            {
                int widthImage = (int)widthSize.Value;
                int heightImage = (int)heightSize.Value;
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);
                colorFlag = true;

                Image<Bgr, byte> resizedImage = inputImage.Resize(widthImage, heightImage, Inter.Linear);
                inputImage = resizedImage;
                if (colorFlag)
                {
                    pictureBox2.Image = inputImage.ToBitmap();
                }
            }



        }

        private void resize_Click(object sender, EventArgs e)
        {
            ResizeImage();
            pictureBox1.Refresh();
        }

        private void bgrGray_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(pictureBox1.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Image<Gray, byte> convertedImage = new Image<Gray, byte>(inputImage.Width, inputImage.Height, new Gray(0));
                convertedImage = inputImage.Convert<Gray, byte>();
                currentImage = convertedImage.Bitmap;
            }
            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Image<Gray, byte> convertedImage = new Image<Gray, byte>(inputImage.Width, inputImage.Height, new Gray(0));
                convertedImage = inputImage.Convert<Gray, byte>();
                currentImage2 = convertedImage.Bitmap;
            }

        }

        public void ApplyCanny(double thresh, double threshLinking)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            Gray grayCannyThreshold = new Gray(80.0);
            Gray grayThreshLinking = new Gray(160.0);

            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(pictureBox1.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Image<Gray, byte> _imgCanny = new Image<Gray, byte>(200, 200, new Gray(0));
                _imgCanny = inputImage.Canny(grayCannyThreshold.Intensity, grayThreshLinking.Intensity);

                Bitmap imageTmp = new Bitmap(_imgCanny.Bitmap.Width, _imgCanny.Bitmap.Height);

                for (int i = 0; i < inputImage.Size.Width; i++)
                {
                    for (int j = 0; j < inputImage.Size.Height; j++)
                    {
                        if (_imgCanny.Bitmap.GetPixel(i, j) == Color.FromArgb(0, 0, 0))
                        {
                            //if black then white
                            digitalstructure.digitalImage[i, j].Color = Color.FromArgb(255, 255, 255);
                            imageTmp.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                        }
                        else
                        {
                            digitalstructure.digitalImage[i, j].Color = Color.FromArgb(0, 0, 0);
                            imageTmp.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                        }
                        if (i == 0 || i == inputImage.Size.Width - 1 || j == 0 || j == inputImage.Size.Height - 1)
                        {
                            digitalstructure.digitalImage[i, j].Color = Color.FromArgb(0, 0, 0);
                            imageTmp.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                        }
                    }
                }
                currentImage = imageTmp;
                //
                //currentImage = _imgCanny.Bitmap;
            }
            else if (radioButton4.Checked)
            {
                pBoxImage2 = new Bitmap(pictureBox2.Image);
                inputImage2 = new Image<Bgr, Byte>(pBoxImage2);

                Image<Gray, byte> _imgCanny2 = new Image<Gray, byte>(200, 200, new Gray(0));
                _imgCanny2 = inputImage2.Canny(grayCannyThreshold.Intensity, grayThreshLinking.Intensity);

                Bitmap imageTmp = new Bitmap(_imgCanny2.Bitmap.Width, _imgCanny2.Bitmap.Height);

                for (int i = 0; i < inputImage.Size.Width; i++)
                {
                    for (int j = 0; j < inputImage.Size.Height; j++)
                    {
                        if (_imgCanny2.Bitmap.GetPixel(i, j) == Color.FromArgb(0, 0, 0))
                        {
                            //if black then white
                            digitalstructure2.digitalImage[i, j].Color = Color.FromArgb(255, 255, 255);
                            imageTmp.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                        }
                        else
                        {
                            digitalstructure2.digitalImage[i, j].Color = Color.FromArgb(0, 0, 0);
                            imageTmp.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                        }

                        if (i == 0 || i == inputImage.Size.Width - 1 || j == 0 || j == inputImage.Size.Height - 1)
                        {
                            digitalstructure2.digitalImage[i, j].Color = Color.FromArgb(0, 0, 0);
                            imageTmp.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                        }

                    }
                }
                currentImage2 = imageTmp;
            }

        }

        private void cannyBtn_Click(object sender, EventArgs e)
        {
            ApplyCanny((double)threshCanny.Value, (double)threshLinkingCanny.Value);
        }

        public void ApplyLaplacian(int aperatureSize)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(pictureBox1.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Image<Gray, byte> _imgGray = inputImage.Convert<Gray, byte>();
                Image<Gray, float> _imgLaplacian = new Image<Gray, float>(200, 200, new Gray(0));
                _imgLaplacian = _imgGray.Laplace(aperatureSize);
                currentImage = _imgLaplacian.Bitmap;
            }
            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Image<Gray, byte> _imgGray = inputImage.Convert<Gray, byte>();
                Image<Gray, float> _imgLaplacian = new Image<Gray, float>(200, 200, new Gray(0));
                _imgLaplacian = _imgGray.Laplace(aperatureSize);
                currentImage2 = _imgLaplacian.Bitmap;
            }

        }

        private void laplacianBtn_Click(object sender, EventArgs e)
        {
            ApplyLaplacian((int)aperatureSize.Value);
        }

        public void ApplySobel()
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(pictureBox1.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Image<Gray, byte> _imgGray = inputImage.Convert<Gray, byte>();
                Image<Gray, float> _imgSobel = new Image<Gray, float>(200, 200, new Gray(0));
                _imgSobel = _imgGray.Sobel(1, 1, 3);
                currentImage = _imgSobel.Bitmap;
            }
            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Image<Gray, byte> _imgGray = inputImage.Convert<Gray, byte>();
                Image<Gray, float> _imgSobel = new Image<Gray, float>(200, 200, new Gray(0));
                _imgSobel = _imgGray.Sobel(1, 1, 3);
                currentImage2 = _imgSobel.Bitmap;
            }

        }

        private void sobelBtn_Click(object sender, EventArgs e)
        {
            ApplySobel();
        }

        private void Binarization()
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(currentImage);
                inputImage1 = new Image<Bgr, Byte>(pBoxImage);

                Image<Gray, byte> _imgGray = inputImage1.Convert<Gray, byte>();
                _imgGray = _imgGray.ThresholdBinary(new Gray(trackBar1.Value), new Gray(255));


                for (int i = 0; i < inputImage1.Size.Width; i++)
                {
                    for (int j = 0; j < inputImage1.Size.Height; j++)
                    {
                        digitalstructure.digitalImage[i, j].Color = _imgGray.Bitmap.GetPixel(i, j);
                    }
                }
                currentImage = _imgGray.ToBitmap();

            }

            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Image<Gray, byte> _imgGray = inputImage.Convert<Gray, byte>();

                _imgGray = _imgGray.ThresholdBinary(new Gray(trackBar1.Value), new Gray(255));


                for (int i = 0; i < inputImage.Size.Width; i++)
                {
                    for (int j = 0; j < inputImage.Size.Height; j++)
                    {
                        digitalstructure2.digitalImage[i, j].Color = _imgGray.Bitmap.GetPixel(i, j);
                    }
                }


                pictureBox2.Image = _imgGray.ToBitmap();
            }
        }


        private void binarizationBtn_Click(object sender, EventArgs e)
        {
            Binarization();
        }

        private void gaussianBtn_Click(object sender, EventArgs e)
        {

            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                Bitmap pBoxImage = new Bitmap(pictureBox1.Image);
                Image<Bgr, byte> inputImage = new Image<Bgr, Byte>(pBoxImage);
                int gaussKernel = (int)gaussianKernel.Value;

                if (gaussKernel % 2 == 0)
                {
                    gaussKernel--;
                }

                inputImage._SmoothGaussian(gaussKernel);
                pictureBox1.Image = inputImage.ToBitmap();
            }
            else if (radioButton4.Checked)
            {
                Bitmap pBoxImage = new Bitmap(pictureBox2.Image);
                Image<Bgr, byte> inputImage = new Image<Bgr, Byte>(pBoxImage);
                int gaussKernel = (int)gaussianKernel.Value;

                if (gaussKernel % 2 == 0)
                {
                    gaussKernel--;
                }

                inputImage._SmoothGaussian(gaussKernel);
                pictureBox2.Image = inputImage.ToBitmap();
            }

        }

        private void bilatralBtn_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                Bitmap pBoxImage = new Bitmap(pictureBox1.Image);
                Image<Bgr, byte> inputImage1 = new Image<Bgr, Byte>(pBoxImage);

                var kernelSize = (int)bilatralKernel.Value;
                var color = (int)colorSigma.Value;
                var space = (int)spaceSigma.Value;
                inputImage1 = inputImage1.SmoothBilateral(kernelSize, color, space);

                for (int i = 0; i < inputImage1.Size.Width; i++)
                {
                    for (int j = 0; j < inputImage1.Size.Height; j++)
                    {
                        digitalstructure.digitalImage[i, j].Color = inputImage1.Bitmap.GetPixel(i, j);
                    }
                }
                currentImage = inputImage1.ToBitmap();
            }
            else if (radioButton4.Checked)
            {
                Bitmap pBoxImage = new Bitmap(pictureBox2.Image);
                Image<Bgr, byte> inputImage = new Image<Bgr, Byte>(pBoxImage);
                var kernelSize = (int)bilatralKernel.Value;
                var color = (int)colorSigma.Value;
                var space = (int)spaceSigma.Value;

                inputImage = inputImage.SmoothBilateral(kernelSize, color, space);
                pictureBox2.Image = inputImage.ToBitmap();
            }
     
        }

        public void ApplyMedianBlur()
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                colorFlag = true;
                Bitmap pBoxImage = new Bitmap(pictureBox1.Image);
                Image<Bgr, byte> inputImage = new Image<Bgr, Byte>(pBoxImage);
                var kernelSize = (int)numericUpDown1.Value;

                inputImage = inputImage.SmoothMedian(kernelSize);

                if (colorFlag)
                {
                    pictureBox1.Image = inputImage.ToBitmap();
                }
            }
            else if (radioButton4.Checked)
            {
                colorFlag = true;
                Bitmap pBoxImage = new Bitmap(pictureBox2.Image);
                Image<Bgr, byte> inputImage = new Image<Bgr, Byte>(pBoxImage);

                inputImage = inputImage.SmoothMedian(3);

                if (colorFlag)
                {
                    pictureBox2.Image = inputImage.ToBitmap();
                }
            }

        }

        private void medianBtn_Click(object sender, EventArgs e)
        {
            ApplyMedianBlur();
        }

        private void resetImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = new Bitmap(pictureBox1.Image);
        }




        private Bitmap Skelatanize(Bitmap image)
        {

            Image<Gray, byte> imgOld = new Image<Gray, byte>(image);
            Image<Gray, byte> img2 = (new Image<Gray, byte>(imgOld.Width, imgOld.Height, new Gray(255))).Sub(imgOld);
            Image<Gray, byte> eroded = new Image<Gray, byte>(img2.Size);
            Image<Gray, byte> temp = new Image<Gray, byte>(img2.Size);
            Image<Gray, byte> skel = new Image<Gray, byte>(img2.Size);
            skel.SetValue(0);
            CvInvoke.Threshold(img2, img2, 127, 256, 0);
            var element = CvInvoke.GetStructuringElement(ElementShape.Cross, new Size(3, 3), new Point(-1, -1));
            bool done = false;

            while (!done)
            {
                CvInvoke.Erode(img2, eroded, element, new Point(-1, -1), 1, BorderType.Reflect, default(MCvScalar));
                CvInvoke.Dilate(eroded, temp, element, new Point(-1, -1), 1, BorderType.Reflect, default(MCvScalar));
                CvInvoke.Subtract(img2, temp, temp);
                CvInvoke.BitwiseOr(skel, temp, skel);
                eroded.CopyTo(img2);
                if (CvInvoke.CountNonZero(img2) == 0) done = true;
            }

            Bitmap imageTmp = new Bitmap(skel.Bitmap.Width, skel.Bitmap.Height);
            pBoxImage = new Bitmap(pictureBox1.Image);
            inputImage1 = new Image<Bgr, Byte>(pBoxImage);

            for (int i = 0; i < inputImage1.Size.Width; i++)
            {
                for (int j = 0; j < inputImage1.Size.Height; j++)
                {
                    if (skel.Bitmap.GetPixel(i, j) == Color.FromArgb(0, 0, 0))
                    {
                        //if black then white
                        digitalstructure.digitalImage[i, j].Color = Color.FromArgb(255, 255, 255);
                        imageTmp.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                    }
                    else
                    {
                        digitalstructure.digitalImage[i, j].Color = Color.FromArgb(0, 0, 0);
                        imageTmp.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                    }
                    if (i == 0 || i == inputImage.Size.Width - 1 || j == 0 || j == inputImage.Size.Height - 1)
                    {
                        digitalstructure.digitalImage[i, j].Color = Color.FromArgb(0, 0, 0);
                        imageTmp.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                    }
                }
            }
            return currentImage = imageTmp;

          
        }

        private void Do_Thinning()
        {
            if (radioButton3.Checked)
            {
                pictureBox1.Image = Skelatanize(pictureBox1.Image as Bitmap);
                MessageBox.Show("Thinning Done!");
            }
            else if (radioButton4.Checked)
            {
                pictureBox2.Image = Skelatanize(pictureBox2.Image as Bitmap);
                MessageBox.Show("Thinning Done!");
            }

        }

        private void skeletizationBtn_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            pictureBox1.Image = Skelatanize(pictureBox1.Image as Bitmap);
            MessageBox.Show("Thinning Done!");
        }
        private void cleanGrains_Click(object sender, EventArgs e)
        {
            Bitmap imageTmp2 = new Bitmap(digitalstructure.width, digitalstructure.height);
            int numOfWhite = 0;
            for (int i = 0; i < digitalstructure.width; i++)
            {
                for (int j = 0; j < digitalstructure.height; j++)
                {
                    if (digitalstructure.digitalImage[i, j].Color != Color.FromArgb(0, 0, 0))
                    {
                        digitalstructure.digitalImage[i, j].Color = Color.FromArgb(255, 255, 255);
                        numOfWhite++;
                    }

                    imageTmp2.SetPixel(i, j, digitalstructure.digitalImage[i, j].Color);
                }
            }
            currentImage2 = currentImage = imageTmp2;
            pictureBox2.Image = currentImage2;
        }



        public List<Cell> GetNeighbour(Cell Point)
        {
            bool IsPointAdd = false;
            List<Cell> resultPoints = new List<Cell>();
            resultPoints.Add(Point);
            int xprev, xnext, yprev, ynext = 0;
            int wUpDown = (int)digitalstructure.width;
            int hUpDown = (int)digitalstructure.height;

            while (true)
            {
                for (int i = 0; i < resultPoints.Count; i++)
                {
                    Cell point = resultPoints[i];
                    IsPointAdd = false;
                    if (point.Ischecked == false)
                    {

                        if (point.X == 0)
                            xprev = wUpDown - 1;
                        else
                            xprev = point.X - 1;

                        if (point.X == wUpDown - 1)
                            xnext = 0;
                        else
                            xnext = point.X + 1;

                        if (point.Y == 0)
                            yprev = hUpDown - 1;
                        else
                            yprev = point.Y - 1;

                        if (point.Y == hUpDown - 1)
                            ynext = 0;
                        else
                            ynext = point.Y + 1;

                        point.Ischecked = true;


                        if (digitalstructure.digitalImage[xprev, point.Y].Color.Equals(Color.FromArgb(255, 255, 255)))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[xprev, point.Y]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[xprev, point.Y]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[xprev, yprev].Color.Equals(Color.FromArgb(255, 255, 255)))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[xprev, yprev]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[xprev, yprev]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[point.X, yprev].Color.Equals(Color.FromArgb(255, 255, 255)))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[point.X, yprev]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[point.X, yprev]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[xnext, yprev].Color.Equals(Color.FromArgb(255, 255, 255)))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[xnext, yprev]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[xnext, yprev]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[xnext, point.Y].Color.Equals(Color.FromArgb(255, 255, 255)))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[xnext, point.Y]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[xnext, point.Y]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[xnext, ynext].Color.Equals(Color.FromArgb(255, 255, 255)))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[xnext, ynext]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[xnext, ynext]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[point.X, ynext].Color.Equals(Color.FromArgb(255, 255, 255)))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[point.X, ynext]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[point.X, ynext]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[xprev, ynext].Color.Equals(Color.FromArgb(255, 255, 255)))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[xprev, ynext]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[xprev, ynext]);
                                IsPointAdd = true;
                            }
                        }
                    }
                }

                if (IsPointAdd == false)
                {
                    break;
                }
            }

            return resultPoints;
        }


        public void ChangeGrainsColor()
        {
            List<Cell> getAllPoints = new List<Cell>();

            int i, j = 0;

            if (radioButton3.Checked)
            {

                for (i = 0; i < digitalstructure.width; i++)
                {
                    for (j = 0; j < digitalstructure.height; j++)
                    {
                        if (digitalstructure.digitalImage[i, j].Color == Color.FromArgb(255, 255, 255))
                        {
                            List<Cell> resultPointsAfterGetNeighbour = new List<Cell>();
                            resultPointsAfterGetNeighbour.Add(new Cell(i, j, Color.FromArgb(255, 255, 255)));
                            getAllPoints = GetNeighbour(new Cell(i, j, Color.FromArgb(255, 255, 255)));

                            int r, g, b, wsp;

                            wsp = rand.Next(1000);
                            r = rand.Next(0, (256 + wsp) + 1) % 256;
                            g = rand.Next(0, (512 + wsp) + 1) % 256;
                            b = rand.Next(0, (1024 + wsp) + 1) % 256;

                            foreach (Cell colorPoint in getAllPoints)
                            {
                                digitalstructure.digitalImage[colorPoint.X, colorPoint.Y].Color = Color.FromArgb(r, g, b);
                            }

                        }

                    }
                }
            }
            else if (radioButton4.Checked)
            {
                for (i = 0; i < digitalstructure2.width; i++)
                {
                    for (j = 0; j < digitalstructure2.height; j++)
                    {
                        if (digitalstructure2.digitalImage[i, j].Color == Color.FromArgb(255, 255, 255))
                        {
                            List<Cell> resultPointsAfterGetNeighbour = new List<Cell>();
                            resultPointsAfterGetNeighbour.Add(new Cell(i, j, Color.FromArgb(255, 255, 255)));
                            getAllPoints = GetNeighbour(new Cell(i, j, Color.FromArgb(255, 255, 255)));

                            int r, g, b, wsp;

                            wsp = rand.Next(1000);
                            r = rand.Next(0, (256 + wsp) + 1) % 256;
                            g = rand.Next(0, (512 + wsp) + 1) % 256;
                            b = rand.Next(0, (1024 + wsp) + 1) % 256;

                            foreach (Cell colorPoint in getAllPoints)
                            {
                                digitalstructure2.digitalImage[colorPoint.X, colorPoint.Y].Color = Color.FromArgb(r, g, b);
                            }

                        }

                    }
                }
            }

            //getEdgesGrain();
            getBitmapFromDigitalStructure();
        }
        public void getEdgesGrain()
        {
            int xprev, xnext, yprev, ynext = 0;
            foreach (var grains in digitalstructure.grains)
            {
                foreach (var point in grains.points)
                {
                    if (point.X == 0)
                        xprev = digitalstructure.width - 1;
                    else
                        xprev = point.X - 1;

                    if (point.X == digitalstructure.width - 1)
                        xnext = 0;
                    else
                        xnext = point.X + 1;

                    if (point.Y == 0)
                        yprev = digitalstructure.height - 1;
                    else
                        yprev = point.Y - 1;

                    if (point.Y == digitalstructure.height - 1)
                        ynext = 0;
                    else
                        ynext = point.Y + 1;

                    if (digitalstructure.digitalImage[xprev, yprev].Color == Color.FromArgb(0, 0, 0)) grains.edges.Add(digitalstructure.digitalImage[xprev, yprev]);
                    if (digitalstructure.digitalImage[xprev, point.Y].Color == Color.FromArgb(0, 0, 0)) grains.edges.Add(digitalstructure.digitalImage[xprev, point.Y]);
                    if (digitalstructure.digitalImage[xprev, ynext].Color == Color.FromArgb(0, 0, 0)) grains.edges.Add(digitalstructure.digitalImage[xprev, ynext]);
                    if (digitalstructure.digitalImage[point.X, yprev].Color == Color.FromArgb(0, 0, 0)) grains.edges.Add(digitalstructure.digitalImage[point.X, yprev]);
                    if (digitalstructure.digitalImage[point.X, ynext].Color == Color.FromArgb(0, 0, 0)) grains.edges.Add(digitalstructure.digitalImage[point.X, ynext]);
                    if (digitalstructure.digitalImage[xnext, yprev].Color == Color.FromArgb(0, 0, 0)) grains.edges.Add(digitalstructure.digitalImage[xnext, yprev]);
                    if (digitalstructure.digitalImage[xnext, point.Y].Color == Color.FromArgb(0, 0, 0)) grains.edges.Add(digitalstructure.digitalImage[xnext, point.Y]);
                    if (digitalstructure.digitalImage[xnext, ynext].Color == Color.FromArgb(0, 0, 0)) grains.edges.Add(digitalstructure.digitalImage[xnext, ynext]);
                }
            }

        }



        public void getBitmapFromDigitalStructure()
        {

            //currentImage = currentImage2 = imageTmp;
            if (radioButton3.Checked)
            {
                Bitmap imageTmp = new Bitmap(digitalstructure.width, digitalstructure.height);

                for (int i = 0; i < digitalstructure.width; i++)
                {
                    for (int j = 0; j < digitalstructure.height; j++)
                    {
                        imageTmp.SetPixel(i, j, digitalstructure.digitalImage[i, j].Color);
                    }
                }
                currentImage = imageTmp;
                //pictureBox1.Image = currentImage;
            }

            else if (radioButton4.Checked)
            {
                Bitmap imageTmp = new Bitmap(digitalstructure2.width, digitalstructure2.height);

                for (int i = 0; i < digitalstructure2.width; i++)
                {
                    for (int j = 0; j < digitalstructure2.height; j++)
                    {
                        imageTmp.SetPixel(i, j, digitalstructure2.digitalImage[i, j].Color);
                    }
                }
                currentImage2 = imageTmp;
                pictureBox2.Image = currentImage2;
            }

        }
        private void BUM_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            ChangeGrainsColor();
            //komentarz
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {


            if (getColors.Checked)
            {
                if (inputImage == null)
                {
                    MessageBox.Show("Please select the image");
                    return;
                }

                MouseEventArgs me = (MouseEventArgs)e;
                int X = me.X;
                int Y = me.Y;
                int pixel = 0;

                for (int i = 0; i < digitalstructure.width; i++)
                {
                    for (int j = 0; j < digitalstructure.height; j++)
                    {
                        if (digitalstructure.digitalImage[i, j].Color == digitalstructure.digitalImage[X, Y].Color)
                        {
                            pixel++;
                        }
                    }
                }
                double prct = ((double)pixel / (((double)digitalstructure.width * (double)digitalstructure.height)) * 100);
                var percentageOfColour = Math.Round(prct, 2).ToString();
                MessageBox.Show(string.Format("Percentage: {0} % {1}", percentageOfColour, 
                                    digitalstructure.digitalImage[X, Y].Color.ToString()));
            }

            pBoxImage = new Bitmap(pictureBox1.Image);
            inputImage = new Image<Bgr, Byte>(pBoxImage);


            if (checkBox1.Checked)
            {
                MouseEventArgs me = (MouseEventArgs)e;
                Graphics g = this.CreateGraphics();
                Point clickedPoint = new Point(me.X, me.Y);
                Graphics draw = Graphics.FromImage(inputImage.Bitmap);

                if (radioButton1.Checked)
                {
                    this.firstPoint = clickedPoint;
                    label21.Text = String.Format("x1 {0} , y1 {1}", this.firstPoint.X, this.firstPoint.Y);

                    SolidBrush brush = new SolidBrush(Color.Red);
                    Point dPoint = new Point(firstPoint.X, (pictureBox1.Height - firstPoint.Y));
                    dPoint.X = firstPoint.X - 2;
                    dPoint.Y = firstPoint.Y - 2;
                    Rectangle rect = new Rectangle(dPoint, new Size(4, 4));
                    draw.FillRectangle(brush, rect);
                    pictureBox1.Image = inputImage.Bitmap;
                }

                if (radioButton2.Checked)
                {
                    this.secondPoint = clickedPoint;
                    label22.Text = String.Format("x2 {0} , y2 {1}", this.secondPoint.X, this.secondPoint.Y);

                    if (!this.firstPoint.IsEmpty && !this.secondPoint.IsEmpty)
                    {

                        this.pictureBox1.Refresh();
                        label20.Text = Math.Round(GetDistance(firstPoint, secondPoint), 2).ToString();

                        SolidBrush brush = new SolidBrush(Color.Red);
                        Point dPoint = new Point(secondPoint.X, (pictureBox1.Height - secondPoint.Y));
                        dPoint.X = secondPoint.X - 2;
                        dPoint.Y = secondPoint.Y - 2;
                        Rectangle rect = new Rectangle(dPoint, new Size(4, 4));
                        draw.FillRectangle(brush, rect);

                        draw.DrawLine(Pens.Red, this.firstPoint, this.secondPoint);

                        pictureBox1.Image = inputImage.Bitmap;
                        this.firstPoint = Point.Empty;
                        this.secondPoint = Point.Empty;
                    }
                }
            }
        }

        private static double GetDistance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow((p2.X - p1.X), 2) + Math.Pow((p2.Y - p1.Y), 2));
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {



            if (getColors.Checked)
            {
                MouseEventArgs me = (MouseEventArgs)e;
                int X = me.X;
                int Y = me.Y;


                int pixel = 0;

                for (int i = 0; i < digitalstructure2.width; i++)
                {
                    for (int j = 0; j < digitalstructure2.height; j++)
                    {

                        //digitalstructure.digitalImage[i, j] = new Cell(i, j, inputImage1.Bitmap.GetPixel(i, j));
                        if (digitalstructure2.digitalImage[i, j].Color == digitalstructure2.digitalImage[X, Y].Color)
                        {
                            pixel++;
                        }


                    }
                }


                double prct = ((double)pixel / (((double)digitalstructure2.width * (double)digitalstructure2.height)) * 100);

                var percentageOfColour = Math.Round(prct, 2).ToString();

                MessageBox.Show(string.Format("Percentage: {0} % {1}", percentageOfColour, digitalstructure2.digitalImage[X, Y].Color.ToString()));
            }





            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            pBoxImage = new Bitmap(pictureBox2.Image);
            inputImage = new Image<Bgr, Byte>(pBoxImage);


            if (checkBox1.Checked)
            {
                MouseEventArgs me = (MouseEventArgs)e;
                Graphics g = this.CreateGraphics();
                Point clickedPoint = new Point(me.X, me.Y);
                Graphics draw = Graphics.FromImage(inputImage.Bitmap);

                if (radioButton1.Checked)
                {
                    this.firstPoint = clickedPoint;
                    label21.Text = String.Format("x1 {0} , y1 {1}", this.firstPoint.X, this.firstPoint.Y);

                    SolidBrush brush = new SolidBrush(Color.Red);
                    Point dPoint = new Point(firstPoint.X, (pictureBox2.Height - firstPoint.Y));
                    dPoint.X = firstPoint.X - 2;
                    dPoint.Y = firstPoint.Y - 2;
                    Rectangle rect = new Rectangle(dPoint, new Size(4, 4));
                    draw.FillRectangle(brush, rect);
                    pictureBox2.Image = inputImage.Bitmap;

                }

                if (radioButton2.Checked)
                {
                    this.secondPoint = clickedPoint;
                    label22.Text = String.Format("x2 {0} , y2 {1}", this.secondPoint.X, this.secondPoint.Y);

                    if (!this.firstPoint.IsEmpty && !this.secondPoint.IsEmpty)
                    {
                        draw.DrawLine(Pens.Red, this.firstPoint, this.secondPoint);

                        this.pictureBox2.Refresh();
                        label20.Text = Math.Round(GetDistance(firstPoint, secondPoint), 2).ToString();

                        SolidBrush brush = new SolidBrush(Color.Red);
                        Point dPoint = new Point(secondPoint.X, (pictureBox2.Height - secondPoint.Y));
                        dPoint.X = secondPoint.X - 2;
                        dPoint.Y = secondPoint.Y - 2;
                        Rectangle rect = new Rectangle(dPoint, new Size(4, 4));
                        draw.FillRectangle(brush, rect);

                        draw.DrawLine(Pens.Red, this.firstPoint, this.secondPoint);

                        pictureBox2.Image = inputImage.Bitmap;
                        this.firstPoint = Point.Empty;
                        this.secondPoint = Point.Empty;
                    }
                }
            }
        }

        private static double GetRealDistance(double px1, double nm1)
        {
            return nm1 / px1;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            double px = Convert.ToDouble(textBox1.Text.ToString());
            double nm = Convert.ToDouble(textBox2.Text.ToString());
            textBox3.Text = Math.Round(GetRealDistance(px, nm), 2).ToString();
        }

        private void drawLines_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(currentImage);
                inputImage1 = new Image<Bgr, Byte>(pBoxImage);

                Graphics draw = Graphics.FromImage(inputImage1.Bitmap);
                Pen pen = new Pen(Color.Black);

                int licznik = 0;

                int loops = (int)numberOfLines.Value;
                int spaces = (int)spaceBetweenLines.Value;

                int startingPoint = spaces;
                Cell tmp;
                tmp = new Cell(0, 0, Color.Blue);

                for (int i = 1; i < loops; i++)
                {

                    if (verticalLines.Checked)
                    {
                        draw.DrawLine(pen, startingPoint + (spaces * i), 0, startingPoint + (spaces * i), digitalstructure.height);

                        for (int k = 0; k < digitalstructure.height; k++)
                        {
                            foreach (Grain grain in digitalstructure.grains)
                            {
                                foreach (Cell cell in grain.edges)
                                {

                                    if (cell.X == startingPoint + (spaces * i) && cell.Y == k)
                                    {
                                        inputImage1.Bitmap.SetPixel(cell.X, cell.Y, Color.Red);
                                        cell.Color = Color.Red;


                                        getAllRedPoints.Add(cell);

                                    }
                                }
                                // tmp=grain.edges.Find(x => x == new Cell(k, startingPoint + (spaces * i),Color.FromArgb(0,0,0)));
                            }

                        }
                    }

                    if (horizontalLines.Checked)
                    {
                        draw.DrawLine(pen, 0, startingPoint + (spaces * i), digitalstructure.width, startingPoint + (spaces * i));

                        for (int k = 0; k < digitalstructure.width; k++)
                        {
                            foreach (Grain grain in digitalstructure.grains)
                            {
                                foreach (Cell cell in grain.edges)
                                {

                                    if (cell.X == k && cell.Y == startingPoint + (spaces * i))
                                    {
                                        inputImage1.Bitmap.SetPixel(cell.X, cell.Y, Color.Red);
                                        cell.Color = Color.Red;


                                        getAllRedPoints.Add(cell);

                                    }
                                }
                                // tmp=grain.edges.Find(x => x == new Cell(k, startingPoint + (spaces * i),Color.FromArgb(0,0,0)));
                            }

                        }
                    }

                    //draw.DrawLine(pen, 0, startingPoint + (spaces * i), digitalstructure.width, startingPoint + (spaces * i));

                    //Cell tmp= digitalstructure.grains.
                    //for(int k= 0; k< digitalstructure.width;k++)
                    //{
                    //        if(digitalstructure.digitalImage[k, startingPoint + (spaces * i)].Color==Color.FromArgb(0,0,0))
                    //        {
                    //            pBoxImage.SetPixel(k, startingPoint + (spaces * i), Color.Red);
                    //        }

                    //}

                }
                //inputImage.Bitmap = pBoxImage;

                for (int i = 0; i < inputImage1.Size.Width; i++)
                {
                    for (int j = 0; j < inputImage1.Size.Height; j++)
                    {
                        if (inputImage1.Bitmap.GetPixel(i, j) == Color.Red) licznik++;
                        digitalstructure.digitalImage[i, j].Color = inputImage1.Bitmap.GetPixel(i, j);
                    }
                }

                currentImage = inputImage1.Bitmap;
            }
            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);


                Graphics draw = Graphics.FromImage(inputImage.Bitmap);
                Pen pen = new Pen(Color.Black);

                int licznik = 0;

                int loops = (int)numberOfLines.Value;
                int spaces = (int)spaceBetweenLines.Value;

                int startingPoint = spaces;

                Cell tmp;
                tmp = new Cell(0, 0, Color.Blue);

                for (int i = 1; i < loops; i++)
                {
                    draw.DrawLine(pen, 0, startingPoint + (spaces * i), digitalstructure2.width, startingPoint + (spaces * i));
                    for (int k = 0; k < digitalstructure2.width; k++)
                    {
                        foreach (Grain grain in digitalstructure2.grains)
                        {
                            foreach (Cell cell in grain.edges)
                            {
                                if (cell.X == k && cell.Y == startingPoint + (spaces * i))
                                {
                                    inputImage.Bitmap.SetPixel(cell.X, cell.Y, Color.Red);
                                    cell.Color = Color.Red;
                                }
                            }
                            // tmp=grain.edges.Find(x => x == new Cell(k, startingPoint + (spaces * i),Color.FromArgb(0,0,0)));
                        }
                        //if (tmp.Color != Color.Blue)
                        //{
                        //    inputImage.Bitmap.SetPixel(k, startingPoint + (spaces * i), Color.Red);
                        //}
                    }
                }
                for (int i = 0; i < inputImage.Size.Width; i++)
                {
                    for (int j = 0; j < inputImage.Size.Height; j++)
                    {
                        if (inputImage.Bitmap.GetPixel(i, j) == Color.Red) licznik++;
                        digitalstructure2.digitalImage[i, j].Color = inputImage.Bitmap.GetPixel(i, j);
                    }
                }

                pictureBox2.Image = inputImage.Bitmap;
            }

        }

        private void button7_Click(object sender, EventArgs e)
        {
           

            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(currentImage);
                inputImage1 = new Image<Bgr, Byte>(pBoxImage);

                int blockSie = Convert.ToInt32(blockSizeControl.Text.ToString());
                Image<Gray, byte> _imgGray = inputImage1.Convert<Gray, byte>();
                _imgGray = _imgGray.ThresholdAdaptive(new Gray(255), AdaptiveThresholdType.GaussianC, 
                                                       ThresholdType.Binary, blockSie, new Gray(0.1));

                for (int i = 0; i < inputImage1.Size.Width; i++)
                {
                    for (int j = 0; j < inputImage1.Size.Height; j++)
                    {
                        digitalstructure.digitalImage[i, j].Color = _imgGray.Bitmap.GetPixel(i, j);
                    }
                }
                currentImage = _imgGray.ToBitmap();

            }
            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(currentImage2);
                inputImage1 = new Image<Bgr, Byte>(pBoxImage);
                Image<Gray, byte> _imgGray = inputImage1.Convert<Gray, byte>();

                Image<Gray, byte> output = _imgGray.ThresholdAdaptive(new Gray(255), AdaptiveThresholdType.GaussianC, ThresholdType.Binary, 79, new Gray(0.1));

                currentImage2 = output.ToBitmap();
            }

        }

        private void getDualPhase_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            DualPhase();
        }

        private void DualPhase()
        {
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(pictureBox1.Image);

                inputImage2 = new Image<Bgr, Byte>(pBoxImage);

                List<Cell> dualPhase = new List<Cell>();
                Cell blackPixel = new Cell();

                for (int i = 0; i < inputImage2.Width; i++)
                {
                    for (int j = 0; j < inputImage2.Height; j++)
                    {
                        blackPixel = new Cell(i, j, inputImage2.Bitmap.GetPixel(i, j));
                        if (blackPixel.Color == Color.FromArgb(0, 0, 0))
                        {
                            dualPhase.Add(blackPixel);
                        }
                    }
                }
                pBoxImage1 = new Bitmap(pictureBox2.Image);

                inputImage1 = new Image<Bgr, Byte>(pBoxImage1);

                foreach (Cell cell in dualPhase)
                {
                    digitalstructure.digitalImage[cell.X, cell.Y].Color = cell.Color;
                    inputImage1.Bitmap.SetPixel(cell.X, cell.Y, Color.FromArgb(0, 0, 0));

                }
                for (int i = 0; i < inputImage1.Size.Width; i++)
                {
                    for (int j = 0; j < inputImage1.Size.Height; j++)
                    {
                        digitalstructure.digitalImage[i, j].Color = inputImage1.Bitmap.GetPixel(i, j);
                    }
                }
                pictureBox2.Image = inputImage1.Bitmap;

            }

            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);

                inputImage2 = new Image<Bgr, Byte>(pBoxImage);

                List<Cell> dualPhase = new List<Cell>();
                Cell blackPixel = new Cell();

                for (int i = 0; i < inputImage2.Width; i++)
                {
                    for (int j = 0; j < inputImage2.Height; j++)
                    {
                        blackPixel = new Cell(i, j, inputImage2.Bitmap.GetPixel(i, j));
                        if (blackPixel.Color == Color.FromArgb(0, 0, 0))
                        {
                            dualPhase.Add(blackPixel);
                        }
                    }
                }
                pBoxImage1 = new Bitmap(pictureBox1.Image);

                inputImage1 = new Image<Bgr, Byte>(pBoxImage1);

                foreach (Cell cell in dualPhase)
                {
                    digitalstructure2.digitalImage[cell.X, cell.Y].Color = cell.Color;
                    inputImage1.Bitmap.SetPixel(cell.X, cell.Y, Color.FromArgb(0, 0, 0));

                }
                for (int i = 0; i < inputImage1.Size.Width; i++)
                {
                    for (int j = 0; j < inputImage1.Size.Height; j++)
                    {
                        digitalstructure.digitalImage[i, j].Color = inputImage1.Bitmap.GetPixel(i, j);
                    }
                }

                pictureBox1.Image = inputImage1.Bitmap;
            }
        }

        //private void tabPage2_Click(object sender, EventArgs e)
        //{
        //    //pictureBox2.Visible = true;


        //    Bitmap imageTmp = new Bitmap(inputImage.Size.Width, inputImage.Size.Height);
        //    digitalstructure = new DigitalStructure(inputImage.Size.Width, inputImage.Size.Height);

        //    for (int i = 0; i < inputImage.Size.Width; i++)
        //    {
        //        for (int j = 0; j < inputImage.Size.Height; j++)
        //        {
        //            digitalstructure.digitalImage[i, j] = new Cell(i, j, inputImage.Bitmap.GetPixel(i, j));
        //            imageTmp.SetPixel(i, j, digitalstructure.digitalImage[i, j].Color);

        //            if (i == 0 || i == inputImage.Size.Width - 1 || j == 0 || j == inputImage.Size.Height - 1)
        //            {
        //                digitalstructure.digitalImage[i, j].Color = Color.FromArgb(0, 0, 0);
        //                imageTmp.SetPixel(i, j, Color.FromArgb(0, 0, 0));
        //            }

        //        }
        //    }


        //}

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = e.Location;//we assign the lastPoint to the current mouse position: e.Location ('e' is from the MouseEventArgs passed into the MouseDown event)

            isMouseDown = true;
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            if (radioButton4.Checked)
            {
                if (checkBox2.Checked)
                {
                    pBoxImage = new Bitmap(pictureBox2.Image);
                    inputImage = new Image<Bgr, Byte>(pBoxImage);


                    if (isMouseDown == true)//check to see if the mouse button is down
                    {

                        if (lastPoint != null)//if our last point is not null, which in this case we have assigned above

                        {
                            using (Graphics g = Graphics.FromImage(inputImage.Bitmap))
                            {
                                g.DrawLine(new Pen(Color.Black, 2), lastPoint, e.Location);
                                g.SmoothingMode = SmoothingMode.AntiAlias;
                                pictureBox2.Image = inputImage.Bitmap;
                            }
                            this.pictureBox2.Refresh();

                            lastPoint = e.Location;//keep assigning the lastPoint to the current mouse position

                        }

                    }
                }
            }


        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;

            lastPoint = Point.Empty;

        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (radioButton3.Checked)
            {
                if (checkBox2.Checked)
                {
                    pBoxImage = new Bitmap(pictureBox1.Image);
                    inputImage = new Image<Bgr, Byte>(pBoxImage);

                    if (isMouseDown == true)//check to see if the mouse button is down
                    {
                        if (lastPoint != null)//if our last point is not null, which in this case we have assigned above
                        {
                            using (Graphics g = Graphics.FromImage(inputImage.Bitmap))

                            {
                                g.DrawLine(new Pen(Color.Black, 3), lastPoint, e.Location);
                                g.SmoothingMode = SmoothingMode.HighQuality;
                            }
                            pictureBox1.Image = inputImage.ToBitmap();
                            this.pictureBox1.Refresh();
                            lastPoint = e.Location;//keep assigning the lastPoint to the current mouse position

                        }
                    }

                }
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;

            lastPoint = Point.Empty;
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = e.Location;//we assign the lastPoint to the current mouse position: e.Location ('e' is from the MouseEventArgs passed into the MouseDown event)

            isMouseDown = true;
        }


        private void openMorph_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(pictureBox1.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));

                inputImage._MorphologyEx(MorphOp.Open, kernel, new Point(-1, -1), 1,
                    BorderType.Default, new MCvScalar(1.0));

                pictureBox1.Image = DigitalStuctureUpdate(inputImage.ToBitmap());
            }
            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));
                inputImage._MorphologyEx(MorphOp.Open, kernel, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(1.0));

                pictureBox2.Image = DigitalStuctureUpdate(inputImage.ToBitmap());
            }

        }

        private void closeMorph_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(pictureBox1.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));

                inputImage._MorphologyEx(MorphOp.Close, kernel, new Point(-1, -1), 1, 
                    BorderType.Default, new MCvScalar(1.0));

                pictureBox1.Image = DigitalStuctureUpdate(inputImage.ToBitmap());
            }
            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));
                inputImage._MorphologyEx(MorphOp.Close, kernel, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(1.0));

                pictureBox2.Image = DigitalStuctureUpdate(inputImage.ToBitmap());
            }
        }

        private void topHatMorph_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(pictureBox1.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));
                inputImage._MorphologyEx(MorphOp.Tophat, kernel, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(1.0));

                pictureBox1.Image = DigitalStuctureUpdate(inputImage.ToBitmap());
            }
            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));
                inputImage._MorphologyEx(MorphOp.Tophat, kernel, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(1.0));

                pictureBox2.Image = DigitalStuctureUpdate(inputImage.ToBitmap());
            }
        }

        private void blackHatMorph_Click(object sender, EventArgs e)
        {
            if (inputImage == null)
            {
                MessageBox.Show("Please select the image");
                return;
            }
            if (radioButton3.Checked)
            {
                pBoxImage = new Bitmap(pictureBox1.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));
                inputImage._MorphologyEx(MorphOp.Blackhat, kernel, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(1.0));

                pictureBox1.Image = DigitalStuctureUpdate(inputImage.ToBitmap());
            }
            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage = new Image<Bgr, Byte>(pBoxImage);

                Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Rectangle, new Size(5, 5), new Point(-1, -1));
                inputImage._MorphologyEx(MorphOp.Blackhat, kernel, new Point(-1, -1), 1, BorderType.Default, new MCvScalar(1.0));

                pictureBox2.Image = DigitalStuctureUpdate(inputImage.ToBitmap());
            }
        }


        private void button10_Click(object sender, EventArgs e)
        {

            pBoxImage = new Bitmap(pictureBox2.Image);
            inputImage = new Image<Bgr, Byte>(pBoxImage);

            Image<Gray, byte> _imgGray = inputImage.Convert<Gray, byte>();

            histogram.Calculate(new Image<Gray, byte>[] { _imgGray }, false, null);

            Mat m = new Mat();
            histogram.CopyTo(m);

            //histogramBox1.AddHistogram("Black scale Histogram", Color.Gray, m, 256, new float[] { 0, 256 });

            //histogramBox1.Refresh();

        }

        private void BorderExpansion_Click(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                prevCells = digitalstructure.digitalImage;
                currentCells = digitalstructure.digitalImage;

                int iprev, inext, jprev, jnext = 0;


                for (int i = 0; i < digitalstructure.width; i++)
                {
                    if (i == 0)
                        iprev = digitalstructure.width - 1;
                    else
                        iprev = i - 1;

                    if (i == digitalstructure.width - 1)
                        inext = 0;
                    else
                        inext = i + 1;

                    for (int j = 0; j < digitalstructure.height; j++)
                    {

                        // czy komorka jest czarna tylko je rozrastamy
                        if (!currentCells[i, j].Color.Equals(Color.FromArgb(0, 0, 0)))
                        {

                            if (j == 0)
                                jprev = digitalstructure.height - 1;
                            else
                                jprev = j - 1;

                            if (j == digitalstructure.height - 1)
                                jnext = 0;
                            else
                                jnext = j + 1;
                            //mamy kolorowa komorke i j i odpalamy sprawdzanie zmiany koloru dla kazdego z jej sasiadow
                            //changeMooreColor2(i, j, digitalstructure.width, digitalstructure.height);
                            changeMooreColor2(iprev, jprev, digitalstructure.width, digitalstructure.height);
                            changeMooreColor2(iprev, j, digitalstructure.width, digitalstructure.height);
                            changeMooreColor2(iprev, jnext, digitalstructure.width, digitalstructure.height);
                            changeMooreColor2(i, jnext, digitalstructure.width, digitalstructure.height);
                            changeMooreColor2(inext, jnext, digitalstructure.width, digitalstructure.height);
                            changeMooreColor2(inext, j, digitalstructure.width, digitalstructure.height);
                            changeMooreColor2(inext, jprev, digitalstructure.width, digitalstructure.height);
                            changeMooreColor2(i, jprev, digitalstructure.width, digitalstructure.height);
                        }
                    }
                }

                updatePrevCells2();
                digitalstructure.digitalImage = prevCells;
                getBitmapFromDigitalStructure();
            }

            else if (radioButton4.Checked)
            {
                prevCells = digitalstructure2.digitalImage;
                currentCells = digitalstructure2.digitalImage;

                int iprev, inext, jprev, jnext = 0;

                //do
                //{
                for (int i = 0; i < digitalstructure2.width; i++)
                {
                    if (i == 0)
                        iprev = digitalstructure2.width - 1;
                    else
                        iprev = i - 1;

                    if (i == digitalstructure2.width - 1)
                        inext = 0;
                    else
                        inext = i + 1;

                    for (int j = 0; j < digitalstructure2.height; j++)
                    {

                        // czy komorka jest czarna tylko je rozrastamy
                        if (!currentCells[i, j].Color.Equals(Color.FromArgb(0, 0, 0)))
                        {

                            if (j == 0)
                                jprev = digitalstructure2.height - 1;
                            else
                                jprev = j - 1;

                            if (j == digitalstructure2.height - 1)
                                jnext = 0;
                            else
                                jnext = j + 1;
                            //mamy kolorowa komorke i j i odpalamy sprawdzanie zmiany koloru dla kazdego z jej sasiadow
                            //changeMooreColor2(i, j, digitalstructure.width, digitalstructure.height);
                            changeMooreColor2(iprev, jprev, digitalstructure2.width, digitalstructure2.height);
                            changeMooreColor2(iprev, j, digitalstructure2.width, digitalstructure2.height);
                            changeMooreColor2(iprev, jnext, digitalstructure2.width, digitalstructure2.height);
                            changeMooreColor2(i, jnext, digitalstructure2.width, digitalstructure2.height);
                            changeMooreColor2(inext, jnext, digitalstructure2.width, digitalstructure2.height);
                            changeMooreColor2(inext, j, digitalstructure2.width, digitalstructure2.height);
                            changeMooreColor2(inext, jprev, digitalstructure2.width, digitalstructure2.height);
                            changeMooreColor2(i, jprev, digitalstructure2.width, digitalstructure2.height);
                        }
                    }
                }
                //przepisuje z current do prev po wykonaniu step moore
                //} while (updatePrevCells2());
                //digitalstructure2.digitalImage = prevCells;
                //getBitmapFromDigitalStructure();
            }

        }
        void changeMooreColor2(int x, int y, int wUpDown, int hUpDown)
        {
            if (currentCells[x, y].Color.Equals(Color.FromArgb(0, 0, 0)))
            {
                int xprev, xnext, yprev, ynext = 0;
                Dictionary<Color, int> matchColor2 = new Dictionary<Color, int>();
                List<Color> neighbours = new List<Color>();

                if (x == 0)
                    xprev = wUpDown - 1;
                else
                    xprev = x - 1;

                if (x == wUpDown - 1)
                    xnext = 0;
                else
                    xnext = x + 1;

                if (y == 0)
                    yprev = hUpDown - 1;
                else
                    yprev = y - 1;

                if (y == hUpDown - 1)
                    ynext = 0;
                else
                    ynext = y + 1;

                if (!prevCells[xprev, y].Color.Equals(Color.FromArgb(0, 0, 0)))
                {
                    neighbours.Add(prevCells[xprev, y].Color);
                }

                if (!prevCells[xprev, yprev].Color.Equals(Color.FromArgb(0, 0, 0)))
                {
                    neighbours.Add(prevCells[xprev, yprev].Color);
                }

                if (!prevCells[x, yprev].Color.Equals(Color.FromArgb(0, 0, 0)))
                {
                    neighbours.Add(prevCells[x, yprev].Color);
                }

                if (!prevCells[xnext, yprev].Color.Equals(Color.FromArgb(0, 0, 0)))
                {
                    neighbours.Add(prevCells[xnext, yprev].Color);
                }

                if (prevCells[xnext, y].Color.Equals(Color.FromArgb(0, 0, 0)))
                {
                    neighbours.Add(prevCells[xnext, y].Color);
                }

                if (!prevCells[xnext, ynext].Color.Equals(Color.FromArgb(0, 0, 0)))
                {
                    neighbours.Add(prevCells[xnext, ynext].Color);
                }

                if (!prevCells[x, ynext].Color.Equals(Color.FromArgb(0, 0, 0)))
                {
                    neighbours.Add(prevCells[x, ynext].Color);
                }

                if (!prevCells[xprev, ynext].Color.Equals(Color.FromArgb(0, 0, 0)))
                {
                    neighbours.Add(prevCells[xprev, ynext].Color);
                }
                ////sprawdza jeszcze siebie samego
                if (!prevCells[x, y].Color.Equals(Color.FromArgb(0, 0, 0)))
                {
                    neighbours.Add(prevCells[x, y].Color);
                }
                //CHECK COLOR
                foreach (Color color in neighbours)
                {
                    if (!matchColor2.ContainsKey(color))
                        matchColor2.Add(color, 1);
                    else
                        matchColor2[color] = matchColor2[color] + 1;
                }

                Color maxKey = getMaxKey(matchColor2);
                if (matchColor2.Count != 0 && currentCells[x, y].Color.Equals(Color.FromArgb(0, 0, 0)))
                {
                    currentCells[x, y].Color = maxKey;
                }
            }
        }

        public List<Cell> GetNeighbour2(Cell Point)
        {
            List<Cell> resultPoints = new List<Cell>();

            if (radioButton3.Checked)
            {
                bool IsPointAdd = false;

                resultPoints.Add(Point);
                int xprev, xnext, yprev, ynext = 0;
                int wUpDown = (int)digitalstructure.width;
                int hUpDown = (int)digitalstructure.height;

                while (true)
                {
                    for (int i = 0; i < resultPoints.Count; i++)
                    {
                        Cell point = resultPoints[i];
                        IsPointAdd = false;
                        //if (point.Ischecked == false)
                        //{

                        if (point.X == 0)
                            xprev = wUpDown - 1;
                        else
                            xprev = point.X - 1;

                        if (point.X == wUpDown - 1)
                            xnext = 0;
                        else
                            xnext = point.X + 1;

                        if (point.Y == 0)
                            yprev = hUpDown - 1;
                        else
                            yprev = point.Y - 1;

                        if (point.Y == hUpDown - 1)
                            ynext = 0;
                        else
                            ynext = point.Y + 1;

                        point.Ischecked = true;


                        if (digitalstructure.digitalImage[xprev, point.Y].Color.Equals(Point.Color))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[xprev, point.Y]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[xprev, point.Y]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[xprev, yprev].Color.Equals(Point.Color))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[xprev, yprev]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[xprev, yprev]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[point.X, yprev].Color.Equals(Point.Color))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[point.X, yprev]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[point.X, yprev]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[xnext, yprev].Color.Equals(Point.Color))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[xnext, yprev]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[xnext, yprev]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[xnext, point.Y].Color.Equals(Point.Color))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[xnext, point.Y]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[xnext, point.Y]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[xnext, ynext].Color.Equals(Point.Color))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[xnext, ynext]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[xnext, ynext]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[point.X, ynext].Color.Equals(Point.Color))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[point.X, ynext]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[point.X, ynext]);
                                IsPointAdd = true;
                            }
                        }

                        if (digitalstructure.digitalImage[xprev, ynext].Color.Equals(Point.Color))
                        {
                            if (!resultPoints.Contains(digitalstructure.digitalImage[xprev, ynext]))
                            {
                                resultPoints.Add(digitalstructure.digitalImage[xprev, ynext]);
                                IsPointAdd = true;
                            }
                        }

                    }

                    if (IsPointAdd == false)
                    {
                        break;
                    }
                }
            }
            else if (radioButton4.Checked)
            {
                bool IsPointAdd = false;

                resultPoints.Add(Point);
                int xprev, xnext, yprev, ynext = 0;
                int wUpDown = (int)digitalstructure.width;
                int hUpDown = (int)digitalstructure.height;

                while (true)
                {
                    for (int i = 0; i < resultPoints.Count; i++)
                    {
                        Cell point = resultPoints[i];
                        IsPointAdd = false;
                        if (point.Ischecked == false)
                        {

                            if (point.X == 0)
                                xprev = wUpDown - 1;
                            else
                                xprev = point.X - 1;

                            if (point.X == wUpDown - 1)
                                xnext = 0;
                            else
                                xnext = point.X + 1;

                            if (point.Y == 0)
                                yprev = hUpDown - 1;
                            else
                                yprev = point.Y - 1;

                            if (point.Y == hUpDown - 1)
                                ynext = 0;
                            else
                                ynext = point.Y + 1;

                            point.Ischecked = true;


                            if (digitalstructure2.digitalImage[xprev, point.Y].Color.Equals(Point.Color))
                            {
                                if (!resultPoints.Contains(digitalstructure2.digitalImage[xprev, point.Y]))
                                {
                                    resultPoints.Add(digitalstructure2.digitalImage[xprev, point.Y]);
                                    IsPointAdd = true;
                                }
                            }

                            if (digitalstructure2.digitalImage[xprev, yprev].Color.Equals(Point.Color))
                            {
                                if (!resultPoints.Contains(digitalstructure2.digitalImage[xprev, yprev]))
                                {
                                    resultPoints.Add(digitalstructure2.digitalImage[xprev, yprev]);
                                    IsPointAdd = true;
                                }
                            }

                            if (digitalstructure2.digitalImage[point.X, yprev].Color.Equals(Point.Color))
                            {
                                if (!resultPoints.Contains(digitalstructure2.digitalImage[point.X, yprev]))
                                {
                                    resultPoints.Add(digitalstructure2.digitalImage[point.X, yprev]);
                                    IsPointAdd = true;
                                }
                            }

                            if (digitalstructure2.digitalImage[xnext, yprev].Color.Equals(Point.Color))
                            {
                                if (!resultPoints.Contains(digitalstructure2.digitalImage[xnext, yprev]))
                                {
                                    resultPoints.Add(digitalstructure2.digitalImage[xnext, yprev]);
                                    IsPointAdd = true;
                                }
                            }

                            if (digitalstructure2.digitalImage[xnext, point.Y].Color.Equals(Point.Color))
                            {
                                if (!resultPoints.Contains(digitalstructure2.digitalImage[xnext, point.Y]))
                                {
                                    resultPoints.Add(digitalstructure2.digitalImage[xnext, point.Y]);
                                    IsPointAdd = true;
                                }
                            }

                            if (digitalstructure2.digitalImage[xnext, ynext].Color.Equals(Point.Color))
                            {
                                if (!resultPoints.Contains(digitalstructure2.digitalImage[xnext, ynext]))
                                {
                                    resultPoints.Add(digitalstructure2.digitalImage[xnext, ynext]);
                                    IsPointAdd = true;
                                }
                            }

                            if (digitalstructure2.digitalImage[point.X, ynext].Color.Equals(Point.Color))
                            {
                                if (!resultPoints.Contains(digitalstructure2.digitalImage[point.X, ynext]))
                                {
                                    resultPoints.Add(digitalstructure2.digitalImage[point.X, ynext]);
                                    IsPointAdd = true;
                                }
                            }

                            if (digitalstructure2.digitalImage[xprev, ynext].Color.Equals(Point.Color))
                            {
                                if (!resultPoints.Contains(digitalstructure2.digitalImage[xprev, ynext]))
                                {
                                    resultPoints.Add(digitalstructure2.digitalImage[xprev, ynext]);
                                    IsPointAdd = true;
                                }
                            }
                        }
                    }

                    if (IsPointAdd == false)
                    {
                        break;
                    }
                }
            }


            return resultPoints;
        }

        List<Cell> dualphase = new List<Cell>();
        //statystyki
        private void button11_Click(object sender, EventArgs e)
        {
            int i = 0, j = 0, id = 1;
            //int dualphase = 0;
           
            List<Cell> getAllPoints = new List<Cell>();

            if (radioButton3.Checked)
            {



                pBoxImage = new Bitmap(currentImage);
                inputImage1 = new Image<Bgr, Byte>(pBoxImage);


                //
                bool isColorNotExist = true;
                Bitmap imageTmp = new Bitmap(inputImage1.Bitmap.Width, inputImage1.Bitmap.Height);

                for (i = 0; i < inputImage1.Size.Width; i++)
                {
                    for (j = 0; j < inputImage1.Size.Height; j++)
                    {

                        digitalstructure.digitalImage[i, j] = new Cell(i, j, inputImage1.Bitmap.GetPixel(i, j));
                        if (digitalstructure.digitalImage[i, j].Color == Color.FromArgb(0, 0, 0))
                        {
                            dualphase.Add(new Cell(i, j, digitalstructure.digitalImage[i, j].Color));
                        }
                        else
                        {

                            isColorNotExist = true;
                            foreach (Grain grain in digitalstructure.grains)
                            {
                                if (grain.color.Equals(digitalstructure.digitalImage[i, j].Color))
                                {
                                    isColorNotExist = false;
                                }

                            }

                            if (isColorNotExist)
                            {
                                getAllPoints = GetNeighbour2(new Cell(i, j, digitalstructure.digitalImage[i, j].Color));
                                Grain tmp = new Grain(getAllPoints.Count());
                                tmp.points = getAllPoints;
                                tmp.Id = id;
                                id++;
                                tmp.color = digitalstructure.digitalImage[i, j].Color;

                                double prct = ((double)getAllPoints.Count() / (((double)digitalstructure.width * (double)digitalstructure.height)) * 100);

                                tmp.percent = Math.Round(prct, 2);

                                digitalstructure.grains.Add(tmp);

                            }

                        }

                    }
                }
                getEdgesGrain();
            }

            else if (radioButton4.Checked)
            {
                pBoxImage = new Bitmap(pictureBox2.Image);
                inputImage2 = new Image<Bgr, Byte>(pBoxImage);


                //
                bool isColorNotExist = true;
                Bitmap imageTmp = new Bitmap(inputImage1.Bitmap.Width, inputImage1.Bitmap.Height);

                for (i = 0; i < inputImage1.Size.Width; i++)
                {
                    for (j = 0; j < inputImage1.Size.Height; j++)
                    {

                        digitalstructure.digitalImage[i, j] = new Cell(i, j, inputImage1.Bitmap.GetPixel(i, j));
                        if (digitalstructure.digitalImage[i, j].Color == Color.FromArgb(0, 0, 0))
                        {
                            dualphase.Add(new Cell(i, j, digitalstructure.digitalImage[i, j].Color));
                        }
                        else
                        {

                            isColorNotExist = true;
                            foreach (Grain grain in digitalstructure.grains)
                            {
                                if (grain.color.Equals(digitalstructure.digitalImage[i, j].Color))
                                {
                                    isColorNotExist = false;
                                }

                            }

                            if (isColorNotExist)
                            {
                                getAllPoints = GetNeighbour2(new Cell(i, j, digitalstructure.digitalImage[i, j].Color));
                                Grain tmp = new Grain(getAllPoints.Count());
                                tmp.points = getAllPoints;
                                tmp.Id = id;
                                id++;
                                tmp.color = digitalstructure.digitalImage[i, j].Color;
                                tmp.percent = Math.Round((double)getAllPoints.Count() / 
                                                (double)(digitalstructure.width * digitalstructure.height) * 100, 2);
                                digitalstructure.grains.Add(tmp);

                            }

                        }

                    }
                }
            }
        }

        public void percentageDualPhase()
        {
            DualPhaseCells dp = new DualPhaseCells(dualphase.Count());
            dp.blackCells = dualphase;
            dp.percentage = Math.Round((double)dualphase.Count() /
                                               (double)(digitalstructure.width * digitalstructure.height) * 100, 2);
            digitalstructure.blackCellsDual.Add(dp);

        }

        public void GrainSurfaceArea(double pixelSurfaceArea)
        {
            List<Grain> grainsSurface = new List<Grain>();
            double grainSurface = 0;
            foreach (Grain grain in digitalstructure.grains)
            {
                grainSurface = grain.points.Count * Math.Pow(pixelSurfaceArea, 2);

                grain.surfaceArea = Math.Round(grainSurface, 2);
                grainsSurface.Add(grain);
            }
        }


        List<Grain> grainsDiameter = new List<Grain>();
        Dictionary<Range, double> GrainsRange = new Dictionary<Range, double>();
        public void GrainDiameter()
        {

            double grainDiameter = 0;

            foreach (Grain grain in digitalstructure.grains)
            {
                grainDiameter = Math.Sqrt(4 * grain.surfaceArea / 3.14);
                grain.diameter = Math.Round(grainDiameter, 2);
                grainsDiameter.Add(grain);
            }

        }



        Dictionary<double, int> grainsRanges = new Dictionary<double, int>();
        List<Range> listRange = new List<Range>();
        public void Ranges()
        {
            var maxDiameter = Math.Round(grainsDiameter.Select(x => x.diameter).Max(), 1);
            var minDiameter = Math.Round(grainsDiameter.Select(x => x.diameter).Min(), 1);
            double rangeSize = Math.Round((maxDiameter - minDiameter) / 5, 1);
            double tmp = minDiameter;
 
            while (tmp < maxDiameter)
            {
                var r = new Range(tmp, tmp + rangeSize);
                r.min = tmp;
                tmp = Math.Round(tmp + rangeSize, 1);
                r.max = tmp;
                r.idRange = tmp;
                listRange.Add(r);
            }
            List<Grain> grainss = new List<Grain>();

            foreach (Range range in listRange)
            {
                foreach (Grain grain in digitalstructure.grains)
                {

                    if (grain.diameter >= range.min && grain.diameter < range.max)
                    {
                        grain.rangeGrainValue = range.max;
                        grainss.Add(grain);
                    }
                }
                int countGrain = grainss.Where(x => x.rangeGrainValue == range.max).Count();
                grainsRanges.Add(range.idRange, countGrain);
            }
        }


        private void button12_Click(object sender, EventArgs e)
        {
            double getPercentValueFromTextBox = Double.Parse(textBox4.Text);

            if (radioButton3.Checked)
            {
                Bitmap imageTmp = new Bitmap(digitalstructure.width, digitalstructure.height);

                for (int i = 0; i < digitalstructure.width; i++)
                {
                    for (int j = 0; j < digitalstructure.height; j++)
                    {
                        imageTmp.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                    }
                }
                foreach (Grain grains in digitalstructure.grains)
                {
                    if (grains.percent > getPercentValueFromTextBox)
                    {
                        foreach (Cell pixel in grains.points)
                        {
                            imageTmp.SetPixel(pixel.X, pixel.Y, grains.color);
                        }
                    }
                }

                currentImage = imageTmp;
                pictureBox1.Image = currentImage;
            }

            if (radioButton4.Checked)
            {
                Bitmap imageTmp = new Bitmap(digitalstructure2.width, digitalstructure2.height);

                for (int i = 0; i < digitalstructure2.width; i++)
                {
                    for (int j = 0; j < digitalstructure2.height; j++)
                    {
                        imageTmp.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                    }
                }


                foreach (Grain grains in digitalstructure2.grains)
                {
                    if (grains.percent > getPercentValueFromTextBox)
                    {
                        foreach (Cell pixel in grains.points)
                        {
                            imageTmp.SetPixel(pixel.X, pixel.Y, grains.color);
                        }


                    }

                }

                currentImage2 = imageTmp;
                pictureBox2.Image = currentImage2;
            }

        }




        private void DrawChart()
        {

            foreach (var charPoints in grainsRanges)
            {

                chart2.Series["Series1"].Points.AddXY(charPoints.Key, charPoints.Value);
            }
            chart2.Visible = true;

        }


        private void button13_Click(object sender, EventArgs e)
        {
            DrawChart();
        }

        private void button3_Click(object sender, EventArgs e)
        {

            SaveFileDialog sfd = new SaveFileDialog()
            {
                FileName = "Grains percentage",
                Filter = "Text (*.txt)|*.txt"
            };
            sfd.ShowDialog();
            string FilePath = sfd.FileName;

            try
            {
                List<string> linesToWrite = new List<string>();
                linesToWrite.Add(digitalstructure.width + "," + digitalstructure.height);


                foreach (Grain grains in digitalstructure.grains)
                {
                    StringBuilder line = new StringBuilder();
                    line.Append("GRAIN ID ");
                    line.Append(grains.Id);
                    line.Append(", R ");
                    line.Append(grains.color.R);
                    line.Append(", G ");
                    line.Append(grains.color.G);
                    line.Append(", B ");
                    line.Append(grains.color.B);
                    line.Append(", Grain percentage: ");
                    line.Append(grains.percent);
                    linesToWrite.Add(line.ToString());

                }
                System.IO.File.WriteAllLines(@FilePath, linesToWrite.ToArray());
            }
            catch
            {
                MessageBox.Show("Error exporting .txt file");
            }
            finally
            {
                MessageBox.Show("Successfully exported .txt file");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

            SaveFileDialog sfd = new SaveFileDialog()
            {
                FileName = "untitled",
                Filter = "Text (*.txt)|*.txt"
            };
            sfd.ShowDialog();
            string FilePath = sfd.FileName;

            try
            {
                List<string> linesToWrite = new List<string>();
                linesToWrite.Add(digitalstructure.width + "," + digitalstructure.height);

                foreach (Cell cell in getAllRedPoints)
                {
                    StringBuilder line = new StringBuilder();
                    line.Append(cell.X);
                    line.Append(",");
                    line.Append(cell.Y);
                    line.Append(",");
                    line.Append(cell.Color.R);
                    line.Append(",");
                    line.Append(cell.Color.G);
                    line.Append(",");
                    line.Append(cell.Color.B);
                    linesToWrite.Add(line.ToString());
                }


                System.IO.File.WriteAllLines(@FilePath, linesToWrite.ToArray());
            }
            catch
            {
                MessageBox.Show("Error exporting .txt file");
            }
            finally
            {
                MessageBox.Show("Successfully exported .txt file");
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            binarizationValue.Text = trackBar1.Value.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            double pixelSurface = Convert.ToDouble(pixelSurfaceArea.Text.ToString());
            GrainSurfaceArea(pixelSurface);

        }

        private void button14_Click(object sender, EventArgs e)
        {
            GrainDiameter();
        }



        private void button15_Click_1(object sender, EventArgs e)
        {
            Ranges();

        }

        private void button16_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog()
            {
                FileName = "Grains diameter",
                Filter = "Text (*.txt)|*.txt"
            };
            sfd.ShowDialog();
            string FilePath = sfd.FileName;

            try
            {
                List<string> linesToWrite = new List<string>();
                linesToWrite.Add(digitalstructure.width + "," + digitalstructure.height);


                foreach (Grain grains in digitalstructure.grains)
                {
                    StringBuilder line = new StringBuilder();
                    line.Append("GRAIN ID ");
                    line.Append(grains.Id);
                    line.Append(", R ");
                    line.Append(grains.color.R);
                    line.Append(", G ");
                    line.Append(grains.color.G);
                    line.Append(", B ");
                    line.Append(grains.color.B);
                    line.Append(", Grain percentage: ");
                    line.Append(grains.diameter);
                    linesToWrite.Add(line.ToString());

                }
                System.IO.File.WriteAllLines(@FilePath, linesToWrite.ToArray());
            }
            catch
            {
                MessageBox.Show("Error exporting .txt file");
            }
            finally
            {
                MessageBox.Show("Successfully exported .txt file");
            }
        }
    

        private void button10_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog()
            {
                FileName = "Grains surface area",
                Filter = "Text (*.txt)|*.txt"
            };
            sfd.ShowDialog();
            string FilePath = sfd.FileName;

            try
            {
                List<string> linesToWrite = new List<string>();
                linesToWrite.Add(digitalstructure.width + "," + digitalstructure.height);


                foreach (Grain grains in digitalstructure.grains)
                {
                    StringBuilder line = new StringBuilder();
                    line.Append("GRAIN ID:   ");
                    line.Append(grains.Id);
                    line.Append(", R:  ");
                    line.Append(grains.color.R);
                    line.Append(", G:  ");
                    line.Append(grains.color.G);
                    line.Append(", B:   ");
                    line.Append(grains.color.B);
                    line.Append(", Grain surface area: ");
                    line.Append(grains.surfaceArea);
                    linesToWrite.Add(line.ToString());

                }
                System.IO.File.WriteAllLines(@FilePath, linesToWrite.ToArray());
            }
            catch
            {
                MessageBox.Show("Error exporting .txt file");
            }
            finally
            {
                MessageBox.Show("Successfully exported .txt file");
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog()
            {
                FileName = "Grains range",
                Filter = "Text (*.txt)|*.txt"
            };
            sfd.ShowDialog();
            string FilePath = sfd.FileName;

            try
            {
                List<string> linesToWrite = new List<string>();
                linesToWrite.Add(digitalstructure.width + "," + digitalstructure.height);


                foreach (var charPoints in grainsRanges)
                {
                    StringBuilder line = new StringBuilder();
                    line.Append("Range:  ");
                    line.Append(charPoints.Key);
                    line.Append(",  Number of grains: ");
                    line.Append(charPoints.Value);
                    linesToWrite.Add(line.ToString());

                }
                System.IO.File.WriteAllLines(@FilePath, linesToWrite.ToArray());
            }
            catch
            {
                MessageBox.Show("Error exporting .txt file");
            }
            finally
            {
                MessageBox.Show("Successfully exported .txt file");
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            percentageDualPhase();

            SaveFileDialog sfd = new SaveFileDialog()
            {
                FileName = "% dual phase",
                Filter = "Text (*.txt)|*.txt"
            };
            sfd.ShowDialog();
            string FilePath = sfd.FileName;

            try
            {
                List<string> linesToWrite = new List<string>();
                linesToWrite.Add(digitalstructure.width + "," + digitalstructure.height);


                foreach (DualPhaseCells dualP in digitalstructure.blackCellsDual)
                {
                    StringBuilder line = new StringBuilder();
                    line.Append("Percentage of dual phase:  ");
                    line.Append(dualP.percentage);
                    linesToWrite.Add(line.ToString());

                }
                System.IO.File.WriteAllLines(@FilePath, linesToWrite.ToArray());
            }
            catch
            {
                MessageBox.Show("Error exporting .txt file");
            }
            finally
            {
                MessageBox.Show("Successfully exported .txt file");
            }

        }

        private bool updatePrevCells2()
        {
            bool allNotBlack = true;

            for (int i = 0; i < digitalstructure.width; i++)
            {
                for (int j = 0; j < digitalstructure.height; j++)
                {
                    prevCells[i, j].Color = currentCells[i, j].Color;
                    if (prevCells[i, j].Color.Equals(Color.FromArgb(0, 0, 0)))
                        allNotBlack = false;

                }
            }

            return allNotBlack;
        }
    }

}





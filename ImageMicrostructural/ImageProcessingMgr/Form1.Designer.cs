﻿namespace ImageProcessingMgr
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title3 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openBMPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bMPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tXTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cleanSpaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.numeric = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.cleanGrains = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.sobelBtn = new System.Windows.Forms.Button();
            this.aperatureSize = new System.Windows.Forms.NumericUpDown();
            this.laplacianBtn = new System.Windows.Forms.Button();
            this.threshLinkingCanny = new System.Windows.Forms.NumericUpDown();
            this.threshCanny = new System.Windows.Forms.NumericUpDown();
            this.cannyBtn = new System.Windows.Forms.Button();
            this.gbSizeUpDown = new System.Windows.Forms.NumericUpDown();
            this.button2 = new System.Windows.Forms.Button();
            this.skeletizationBtn = new System.Windows.Forms.Button();
            this.binarizationBtn = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.bgrGray = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.resize = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.heightSize = new System.Windows.Forms.NumericUpDown();
            this.widthSize = new System.Windows.Forms.NumericUpDown();
            this.flipVerticaly = new System.Windows.Forms.Button();
            this.flipHorizontally = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.sharpenValue = new System.Windows.Forms.Label();
            this.contrastValue = new System.Windows.Forms.Label();
            this.brightnessValue = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.sharpenTrackBar = new System.Windows.Forms.TrackBar();
            this.contrastTrackBar = new System.Windows.Forms.TrackBar();
            this.brightnessTrackBar = new System.Windows.Forms.TrackBar();
            this.button7 = new System.Windows.Forms.Button();
            this.drawLines = new System.Windows.Forms.Button();
            this.spaceBetweenLines = new System.Windows.Forms.NumericUpDown();
            this.numberOfLines = new System.Windows.Forms.NumericUpDown();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.button9 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.BorderExpansion = new System.Windows.Forms.Button();
            this.getDualPhase = new System.Windows.Forms.Button();
            this.BUM = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.getColors = new System.Windows.Forms.CheckBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.spaceSigma = new System.Windows.Forms.NumericUpDown();
            this.colorSigma = new System.Windows.Forms.NumericUpDown();
            this.bilatralKernel = new System.Windows.Forms.NumericUpDown();
            this.gaussianKernel = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.medianBtn = new System.Windows.Forms.Button();
            this.bilatralBtn = new System.Windows.Forms.Button();
            this.gaussianBtn = new System.Windows.Forms.Button();
            this.blackHatMorph = new System.Windows.Forms.Button();
            this.topHatMorph = new System.Windows.Forms.Button();
            this.closeMorph = new System.Windows.Forms.Button();
            this.openMorph = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.button11 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label28 = new System.Windows.Forms.Label();
            this.blockSizeControl = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.binarizationValue = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.label26 = new System.Windows.Forms.Label();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.pixelSurfaceArea = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.horizontalLines = new System.Windows.Forms.RadioButton();
            this.verticalLines = new System.Windows.Forms.RadioButton();
            this.applyImageSize = new System.Windows.Forms.Button();
            this.trackBar1_horizontal = new System.Windows.Forms.TrackBar();
            this.trackBar1_vertical = new System.Windows.Forms.TrackBar();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button18 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aperatureSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threshLinkingCanny)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.threshCanny)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbSizeUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.widthSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharpenTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contrastTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brightnessTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spaceBetweenLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spaceSigma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorSigma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bilatralKernel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gaussianKernel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1_horizontal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1_vertical)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.cleanSpaceToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(2085, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openImageToolStripMenuItem,
            this.saveImageToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openImageToolStripMenuItem
            // 
            this.openImageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openBMPToolStripMenuItem});
            this.openImageToolStripMenuItem.Name = "openImageToolStripMenuItem";
            this.openImageToolStripMenuItem.Size = new System.Drawing.Size(195, 30);
            this.openImageToolStripMenuItem.Text = "Open Image";
            // 
            // openBMPToolStripMenuItem
            // 
            this.openBMPToolStripMenuItem.Name = "openBMPToolStripMenuItem";
            this.openBMPToolStripMenuItem.Size = new System.Drawing.Size(132, 30);
            this.openBMPToolStripMenuItem.Text = "BMP";
            this.openBMPToolStripMenuItem.Click += new System.EventHandler(this.openBMPToolStripMenuItem_Click);
            // 
            // saveImageToolStripMenuItem
            // 
            this.saveImageToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bMPToolStripMenuItem,
            this.tXTToolStripMenuItem});
            this.saveImageToolStripMenuItem.Name = "saveImageToolStripMenuItem";
            this.saveImageToolStripMenuItem.Size = new System.Drawing.Size(195, 30);
            this.saveImageToolStripMenuItem.Text = "Save Image";
            // 
            // bMPToolStripMenuItem
            // 
            this.bMPToolStripMenuItem.Name = "bMPToolStripMenuItem";
            this.bMPToolStripMenuItem.Size = new System.Drawing.Size(132, 30);
            this.bMPToolStripMenuItem.Text = "BMP";
            this.bMPToolStripMenuItem.Click += new System.EventHandler(this.bMPToolStripMenuItem_Click);
            // 
            // tXTToolStripMenuItem
            // 
            this.tXTToolStripMenuItem.Name = "tXTToolStripMenuItem";
            this.tXTToolStripMenuItem.Size = new System.Drawing.Size(132, 30);
            this.tXTToolStripMenuItem.Text = "TXT";
            // 
            // cleanSpaceToolStripMenuItem
            // 
            this.cleanSpaceToolStripMenuItem.Name = "cleanSpaceToolStripMenuItem";
            this.cleanSpaceToolStripMenuItem.Size = new System.Drawing.Size(117, 29);
            this.cleanSpaceToolStripMenuItem.Text = "Clean space";
            this.cleanSpaceToolStripMenuItem.Click += new System.EventHandler(this.cleanSpaceToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(96, 381);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(471, 598);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(267, 107);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 60);
            this.button1.TabIndex = 8;
            this.button1.Text = "Dilation";
            this.button1.UseMnemonic = false;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // numeric
            // 
            this.numeric.Location = new System.Drawing.Point(251, 48);
            this.numeric.Name = "numeric";
            this.numeric.Size = new System.Drawing.Size(138, 26);
            this.numeric.TabIndex = 9;
            this.numeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(163, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "Iterations";
            // 
            // cleanGrains
            // 
            this.cleanGrains.Location = new System.Drawing.Point(872, 54);
            this.cleanGrains.Name = "cleanGrains";
            this.cleanGrains.Size = new System.Drawing.Size(130, 87);
            this.cleanGrains.TabIndex = 11;
            this.cleanGrains.Text = "Clean Grains ";
            this.cleanGrains.UseVisualStyleBackColor = true;
            this.cleanGrains.Click += new System.EventHandler(this.cleanGrains_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(521, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(115, 20);
            this.label14.TabIndex = 10;
            this.label14.Text = "Aperature Size";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(90, 115);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 20);
            this.label13.TabIndex = 9;
            this.label13.Text = "Thresh Linking";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(135, 58);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 20);
            this.label12.TabIndex = 8;
            this.label12.Text = "Thresh";
            // 
            // sobelBtn
            // 
            this.sobelBtn.Location = new System.Drawing.Point(706, 54);
            this.sobelBtn.Name = "sobelBtn";
            this.sobelBtn.Size = new System.Drawing.Size(124, 87);
            this.sobelBtn.TabIndex = 7;
            this.sobelBtn.Text = "Sobel";
            this.sobelBtn.UseVisualStyleBackColor = true;
            this.sobelBtn.Click += new System.EventHandler(this.sobelBtn_Click);
            // 
            // aperatureSize
            // 
            this.aperatureSize.Location = new System.Drawing.Point(525, 48);
            this.aperatureSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.aperatureSize.Name = "aperatureSize";
            this.aperatureSize.Size = new System.Drawing.Size(108, 26);
            this.aperatureSize.TabIndex = 6;
            this.aperatureSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // laplacianBtn
            // 
            this.laplacianBtn.Location = new System.Drawing.Point(525, 92);
            this.laplacianBtn.Name = "laplacianBtn";
            this.laplacianBtn.Size = new System.Drawing.Size(131, 49);
            this.laplacianBtn.TabIndex = 5;
            this.laplacianBtn.Text = "Laplacian";
            this.laplacianBtn.UseVisualStyleBackColor = true;
            this.laplacianBtn.Click += new System.EventHandler(this.laplacianBtn_Click);
            // 
            // threshLinkingCanny
            // 
            this.threshLinkingCanny.Location = new System.Drawing.Point(226, 115);
            this.threshLinkingCanny.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.threshLinkingCanny.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.threshLinkingCanny.Name = "threshLinkingCanny";
            this.threshLinkingCanny.Size = new System.Drawing.Size(108, 26);
            this.threshLinkingCanny.TabIndex = 4;
            this.threshLinkingCanny.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // threshCanny
            // 
            this.threshCanny.Location = new System.Drawing.Point(226, 59);
            this.threshCanny.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.threshCanny.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.threshCanny.Name = "threshCanny";
            this.threshCanny.Size = new System.Drawing.Size(108, 26);
            this.threshCanny.TabIndex = 3;
            this.threshCanny.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // cannyBtn
            // 
            this.cannyBtn.Location = new System.Drawing.Point(361, 59);
            this.cannyBtn.Name = "cannyBtn";
            this.cannyBtn.Size = new System.Drawing.Size(134, 82);
            this.cannyBtn.TabIndex = 2;
            this.cannyBtn.Text = "Canny";
            this.cannyBtn.UseVisualStyleBackColor = true;
            this.cannyBtn.Click += new System.EventHandler(this.cannyBtn_Click);
            // 
            // gbSizeUpDown
            // 
            this.gbSizeUpDown.Location = new System.Drawing.Point(1129, 48);
            this.gbSizeUpDown.Name = "gbSizeUpDown";
            this.gbSizeUpDown.Size = new System.Drawing.Size(111, 26);
            this.gbSizeUpDown.TabIndex = 1;
            this.gbSizeUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.gbSizeUpDown.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1009, 89);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(248, 52);
            this.button2.TabIndex = 0;
            this.button2.Text = "Bounderies coloring";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // skeletizationBtn
            // 
            this.skeletizationBtn.Location = new System.Drawing.Point(616, 28);
            this.skeletizationBtn.Name = "skeletizationBtn";
            this.skeletizationBtn.Size = new System.Drawing.Size(293, 139);
            this.skeletizationBtn.TabIndex = 1;
            this.skeletizationBtn.Text = "Skeletonization";
            this.skeletizationBtn.UseVisualStyleBackColor = true;
            this.skeletizationBtn.Click += new System.EventHandler(this.skeletizationBtn_Click);
            // 
            // binarizationBtn
            // 
            this.binarizationBtn.Location = new System.Drawing.Point(74, 28);
            this.binarizationBtn.Name = "binarizationBtn";
            this.binarizationBtn.Size = new System.Drawing.Size(248, 47);
            this.binarizationBtn.TabIndex = 0;
            this.binarizationBtn.Text = "Binarization";
            this.binarizationBtn.UseVisualStyleBackColor = true;
            this.binarizationBtn.Click += new System.EventHandler(this.binarizationBtn_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(249, 101);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(49, 20);
            this.label22.TabIndex = 28;
            this.label22.Text = "x2, y2";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(249, 58);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(49, 20);
            this.label21.TabIndex = 27;
            this.label21.Text = "x1, y1";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(249, 139);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(18, 20);
            this.label20.TabIndex = 26;
            this.label20.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(133, 139);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 20);
            this.label17.TabIndex = 25;
            this.label17.Text = "Distance:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(21, 14);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(193, 24);
            this.checkBox1.TabIndex = 24;
            this.checkBox1.Text = "Get the mesure points";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(86, 101);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(129, 24);
            this.radioButton2.TabIndex = 23;
            this.radioButton2.Text = "Second Point";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(110, 58);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(105, 24);
            this.radioButton1.TabIndex = 22;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "First Point";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // bgrGray
            // 
            this.bgrGray.Location = new System.Drawing.Point(820, 50);
            this.bgrGray.Name = "bgrGray";
            this.bgrGray.Size = new System.Drawing.Size(149, 76);
            this.bgrGray.TabIndex = 27;
            this.bgrGray.Text = " Gray";
            this.bgrGray.UseVisualStyleBackColor = true;
            this.bgrGray.Click += new System.EventHandler(this.bgrGray_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(816, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(153, 20);
            this.label11.TabIndex = 26;
            this.label11.Text = "Color transformation";
            // 
            // resize
            // 
            this.resize.Location = new System.Drawing.Point(315, 50);
            this.resize.Name = "resize";
            this.resize.Size = new System.Drawing.Size(107, 76);
            this.resize.TabIndex = 28;
            this.resize.Text = "Resize ";
            this.resize.UseVisualStyleBackColor = true;
            this.resize.Click += new System.EventHandler(this.resize_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(99, 102);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 20);
            this.label10.TabIndex = 25;
            this.label10.Text = "Height";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(105, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 20);
            this.label9.TabIndex = 24;
            this.label9.Text = "Width";
            // 
            // heightSize
            // 
            this.heightSize.Location = new System.Drawing.Point(170, 100);
            this.heightSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.heightSize.Name = "heightSize";
            this.heightSize.Size = new System.Drawing.Size(120, 26);
            this.heightSize.TabIndex = 23;
            // 
            // widthSize
            // 
            this.widthSize.Location = new System.Drawing.Point(170, 50);
            this.widthSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.widthSize.Name = "widthSize";
            this.widthSize.Size = new System.Drawing.Size(120, 26);
            this.widthSize.TabIndex = 22;
            // 
            // flipVerticaly
            // 
            this.flipVerticaly.Location = new System.Drawing.Point(597, 50);
            this.flipVerticaly.Name = "flipVerticaly";
            this.flipVerticaly.Size = new System.Drawing.Size(96, 76);
            this.flipVerticaly.TabIndex = 20;
            this.flipVerticaly.Text = "Fllip ↑↓";
            this.flipVerticaly.UseVisualStyleBackColor = true;
            this.flipVerticaly.Click += new System.EventHandler(this.flipVerticaly_Click);
            // 
            // flipHorizontally
            // 
            this.flipHorizontally.Location = new System.Drawing.Point(474, 50);
            this.flipHorizontally.Name = "flipHorizontally";
            this.flipHorizontally.Size = new System.Drawing.Size(107, 76);
            this.flipHorizontally.TabIndex = 19;
            this.flipHorizontally.Text = "Flip ⇆\r\n";
            this.flipHorizontally.UseVisualStyleBackColor = true;
            this.flipHorizontally.Click += new System.EventHandler(this.flipHorizontally_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(708, 50);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(95, 76);
            this.button6.TabIndex = 18;
            this.button6.Text = "↻ 90°";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // sharpenValue
            // 
            this.sharpenValue.AutoSize = true;
            this.sharpenValue.Location = new System.Drawing.Point(992, 132);
            this.sharpenValue.Name = "sharpenValue";
            this.sharpenValue.Size = new System.Drawing.Size(18, 20);
            this.sharpenValue.TabIndex = 8;
            this.sharpenValue.Text = "0";
            // 
            // contrastValue
            // 
            this.contrastValue.AutoSize = true;
            this.contrastValue.Location = new System.Drawing.Point(992, 73);
            this.contrastValue.Name = "contrastValue";
            this.contrastValue.Size = new System.Drawing.Size(18, 20);
            this.contrastValue.TabIndex = 7;
            this.contrastValue.Text = "0";
            // 
            // brightnessValue
            // 
            this.brightnessValue.AutoSize = true;
            this.brightnessValue.Location = new System.Drawing.Point(992, 15);
            this.brightnessValue.Name = "brightnessValue";
            this.brightnessValue.Size = new System.Drawing.Size(18, 20);
            this.brightnessValue.TabIndex = 6;
            this.brightnessValue.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(161, 132);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 20);
            this.label8.TabIndex = 5;
            this.label8.Text = "Sharpen";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(161, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "Contrast";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(152, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 20);
            this.label6.TabIndex = 3;
            this.label6.Text = "Brightness";
            // 
            // sharpenTrackBar
            // 
            this.sharpenTrackBar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.sharpenTrackBar.Location = new System.Drawing.Point(260, 132);
            this.sharpenTrackBar.Maximum = 100;
            this.sharpenTrackBar.Name = "sharpenTrackBar";
            this.sharpenTrackBar.Size = new System.Drawing.Size(714, 69);
            this.sharpenTrackBar.TabIndex = 2;
            this.sharpenTrackBar.Scroll += new System.EventHandler(this.sharpenTrackBar_Scroll);
            // 
            // contrastTrackBar
            // 
            this.contrastTrackBar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.contrastTrackBar.Location = new System.Drawing.Point(252, 73);
            this.contrastTrackBar.Maximum = 100;
            this.contrastTrackBar.Minimum = 10;
            this.contrastTrackBar.Name = "contrastTrackBar";
            this.contrastTrackBar.Size = new System.Drawing.Size(714, 69);
            this.contrastTrackBar.TabIndex = 1;
            this.contrastTrackBar.Value = 20;
            this.contrastTrackBar.Scroll += new System.EventHandler(this.contrastTrackBar_Scroll);
            // 
            // brightnessTrackBar
            // 
            this.brightnessTrackBar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.brightnessTrackBar.Location = new System.Drawing.Point(252, 15);
            this.brightnessTrackBar.Maximum = 100;
            this.brightnessTrackBar.Name = "brightnessTrackBar";
            this.brightnessTrackBar.Size = new System.Drawing.Size(714, 69);
            this.brightnessTrackBar.TabIndex = 0;
            this.brightnessTrackBar.Scroll += new System.EventHandler(this.brightnessTrackBar_Scroll);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(74, 95);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(248, 48);
            this.button7.TabIndex = 13;
            this.button7.Text = "Threshold Adaptive";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // drawLines
            // 
            this.drawLines.Location = new System.Drawing.Point(877, 117);
            this.drawLines.Name = "drawLines";
            this.drawLines.Size = new System.Drawing.Size(238, 45);
            this.drawLines.TabIndex = 12;
            this.drawLines.Text = "Draw Lines";
            this.drawLines.UseVisualStyleBackColor = true;
            this.drawLines.Click += new System.EventHandler(this.drawLines_Click);
            // 
            // spaceBetweenLines
            // 
            this.spaceBetweenLines.Location = new System.Drawing.Point(1043, 78);
            this.spaceBetweenLines.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.spaceBetweenLines.Name = "spaceBetweenLines";
            this.spaceBetweenLines.Size = new System.Drawing.Size(72, 26);
            this.spaceBetweenLines.TabIndex = 11;
            this.spaceBetweenLines.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numberOfLines
            // 
            this.numberOfLines.Location = new System.Drawing.Point(1045, 27);
            this.numberOfLines.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numberOfLines.Name = "numberOfLines";
            this.numberOfLines.Size = new System.Drawing.Size(70, 26);
            this.numberOfLines.TabIndex = 10;
            this.numberOfLines.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(1029, 55);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(72, 24);
            this.checkBox2.TabIndex = 43;
            this.checkBox2.Text = "Draw";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(744, 50);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(173, 92);
            this.button9.TabIndex = 42;
            this.button9.Text = "Measure";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(971, 50);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(76, 20);
            this.label25.TabIndex = 41;
            this.label25.Text = "1px [ μm ]";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(549, 119);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 20);
            this.label24.TabIndex = 40;
            this.label24.Text = "μm";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(483, 56);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(105, 20);
            this.label23.TabIndex = 39;
            this.label23.Text = "Pixel distance";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(975, 101);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(122, 26);
            this.textBox3.TabIndex = 38;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(596, 116);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(125, 26);
            this.textBox2.TabIndex = 37;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(596, 50);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(125, 26);
            this.textBox1.TabIndex = 36;
            // 
            // BorderExpansion
            // 
            this.BorderExpansion.Location = new System.Drawing.Point(515, 69);
            this.BorderExpansion.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BorderExpansion.Name = "BorderExpansion";
            this.BorderExpansion.Size = new System.Drawing.Size(227, 66);
            this.BorderExpansion.TabIndex = 24;
            this.BorderExpansion.Text = "Border expansion";
            this.BorderExpansion.UseVisualStyleBackColor = true;
            this.BorderExpansion.Click += new System.EventHandler(this.BorderExpansion_Click);
            // 
            // getDualPhase
            // 
            this.getDualPhase.Location = new System.Drawing.Point(780, 69);
            this.getDualPhase.Name = "getDualPhase";
            this.getDualPhase.Size = new System.Drawing.Size(227, 66);
            this.getDualPhase.TabIndex = 14;
            this.getDualPhase.Text = "Get Dual Phase";
            this.getDualPhase.UseVisualStyleBackColor = true;
            this.getDualPhase.Click += new System.EventHandler(this.getDualPhase_Click);
            // 
            // BUM
            // 
            this.BUM.Location = new System.Drawing.Point(254, 69);
            this.BUM.Name = "BUM";
            this.BUM.Size = new System.Drawing.Size(227, 66);
            this.BUM.TabIndex = 21;
            this.BUM.Text = "Change grains colors";
            this.BUM.UseVisualStyleBackColor = true;
            this.BUM.Click += new System.EventHandler(this.BUM_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(744, 26);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(128, 109);
            this.button13.TabIndex = 5;
            this.button13.Text = "Get Chart";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(944, 93);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(254, 42);
            this.button12.TabIndex = 3;
            this.button12.Text = "Display Grains";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(1098, 34);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 26);
            this.textBox4.TabIndex = 2;
            // 
            // getColors
            // 
            this.getColors.AutoSize = true;
            this.getColors.Location = new System.Drawing.Point(758, 128);
            this.getColors.Name = "getColors";
            this.getColors.Size = new System.Drawing.Size(111, 24);
            this.getColors.TabIndex = 0;
            this.getColors.Text = "Get Colors";
            this.getColors.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(694, 14);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 20);
            this.label19.TabIndex = 12;
            this.label19.Text = "Sigma";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(592, 14);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 20);
            this.label18.TabIndex = 11;
            this.label18.Text = "Color Sigma";
            // 
            // spaceSigma
            // 
            this.spaceSigma.Location = new System.Drawing.Point(688, 54);
            this.spaceSigma.Minimum = new decimal(new int[] {
            55,
            0,
            0,
            0});
            this.spaceSigma.Name = "spaceSigma";
            this.spaceSigma.Size = new System.Drawing.Size(76, 26);
            this.spaceSigma.TabIndex = 10;
            this.spaceSigma.Value = new decimal(new int[] {
            55,
            0,
            0,
            0});
            // 
            // colorSigma
            // 
            this.colorSigma.Location = new System.Drawing.Point(597, 54);
            this.colorSigma.Minimum = new decimal(new int[] {
            55,
            0,
            0,
            0});
            this.colorSigma.Name = "colorSigma";
            this.colorSigma.Size = new System.Drawing.Size(86, 26);
            this.colorSigma.TabIndex = 9;
            this.colorSigma.Value = new decimal(new int[] {
            55,
            0,
            0,
            0});
            // 
            // bilatralKernel
            // 
            this.bilatralKernel.Location = new System.Drawing.Point(502, 54);
            this.bilatralKernel.Minimum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.bilatralKernel.Name = "bilatralKernel";
            this.bilatralKernel.Size = new System.Drawing.Size(82, 26);
            this.bilatralKernel.TabIndex = 7;
            this.bilatralKernel.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // gaussianKernel
            // 
            this.gaussianKernel.Location = new System.Drawing.Point(352, 56);
            this.gaussianKernel.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.gaussianKernel.Name = "gaussianKernel";
            this.gaussianKernel.Size = new System.Drawing.Size(100, 26);
            this.gaussianKernel.TabIndex = 6;
            this.gaussianKernel.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(498, 14);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 20);
            this.label16.TabIndex = 4;
            this.label16.Text = "Kernel Size";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(185, 62);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(161, 20);
            this.label15.TabIndex = 3;
            this.label15.Text = "Gaussian Kernel Size";
            // 
            // medianBtn
            // 
            this.medianBtn.Location = new System.Drawing.Point(830, 56);
            this.medianBtn.Name = "medianBtn";
            this.medianBtn.Size = new System.Drawing.Size(179, 96);
            this.medianBtn.TabIndex = 2;
            this.medianBtn.Text = "Median";
            this.medianBtn.UseVisualStyleBackColor = true;
            this.medianBtn.Click += new System.EventHandler(this.medianBtn_Click);
            // 
            // bilatralBtn
            // 
            this.bilatralBtn.Location = new System.Drawing.Point(503, 103);
            this.bilatralBtn.Name = "bilatralBtn";
            this.bilatralBtn.Size = new System.Drawing.Size(262, 49);
            this.bilatralBtn.TabIndex = 1;
            this.bilatralBtn.Text = "Bilateral";
            this.bilatralBtn.UseVisualStyleBackColor = true;
            this.bilatralBtn.Click += new System.EventHandler(this.bilatralBtn_Click);
            // 
            // gaussianBtn
            // 
            this.gaussianBtn.Location = new System.Drawing.Point(190, 107);
            this.gaussianBtn.Name = "gaussianBtn";
            this.gaussianBtn.Size = new System.Drawing.Size(262, 45);
            this.gaussianBtn.TabIndex = 0;
            this.gaussianBtn.Text = "Apply Gaussian";
            this.gaussianBtn.UseVisualStyleBackColor = true;
            this.gaussianBtn.Click += new System.EventHandler(this.gaussianBtn_Click);
            // 
            // blackHatMorph
            // 
            this.blackHatMorph.Location = new System.Drawing.Point(616, 99);
            this.blackHatMorph.Name = "blackHatMorph";
            this.blackHatMorph.Size = new System.Drawing.Size(140, 68);
            this.blackHatMorph.TabIndex = 16;
            this.blackHatMorph.Text = "Black Hat";
            this.blackHatMorph.UseMnemonic = false;
            this.blackHatMorph.UseVisualStyleBackColor = true;
            this.blackHatMorph.Visible = false;
            this.blackHatMorph.Click += new System.EventHandler(this.blackHatMorph_Click);
            // 
            // topHatMorph
            // 
            this.topHatMorph.Location = new System.Drawing.Point(616, 28);
            this.topHatMorph.Name = "topHatMorph";
            this.topHatMorph.Size = new System.Drawing.Size(140, 65);
            this.topHatMorph.TabIndex = 15;
            this.topHatMorph.Text = "Top Hat";
            this.topHatMorph.UseMnemonic = false;
            this.topHatMorph.UseVisualStyleBackColor = true;
            this.topHatMorph.Visible = false;
            this.topHatMorph.Click += new System.EventHandler(this.topHatMorph_Click);
            // 
            // closeMorph
            // 
            this.closeMorph.Location = new System.Drawing.Point(480, 99);
            this.closeMorph.Name = "closeMorph";
            this.closeMorph.Size = new System.Drawing.Size(130, 68);
            this.closeMorph.TabIndex = 14;
            this.closeMorph.Text = "Close";
            this.closeMorph.UseMnemonic = false;
            this.closeMorph.UseVisualStyleBackColor = true;
            this.closeMorph.Click += new System.EventHandler(this.closeMorph_Click);
            // 
            // openMorph
            // 
            this.openMorph.Location = new System.Drawing.Point(480, 28);
            this.openMorph.Name = "openMorph";
            this.openMorph.Size = new System.Drawing.Size(130, 65);
            this.openMorph.TabIndex = 13;
            this.openMorph.Text = "Open";
            this.openMorph.UseMnemonic = false;
            this.openMorph.UseVisualStyleBackColor = true;
            this.openMorph.Click += new System.EventHandler(this.openMorph_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(131, 107);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(130, 60);
            this.button8.TabIndex = 12;
            this.button8.Text = "Erosion";
            this.button8.UseMnemonic = false;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(1327, 363);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(718, 789);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 20;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
            this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseMove);
            this.pictureBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(327, 325);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(92, 24);
            this.radioButton3.TabIndex = 22;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Image 1";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(1661, 307);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(92, 24);
            this.radioButton4.TabIndex = 23;
            this.radioButton4.Text = "Image 2";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.Visible = false;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(40, 25);
            this.button11.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(200, 110);
            this.button11.TabIndex = 25;
            this.button11.Text = "Get Statistic";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(945, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 20);
            this.label1.TabIndex = 26;
            this.label1.Text = "Fill the percentage";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(915, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 20);
            this.label3.TabIndex = 27;
            this.label3.Text = "Number of lines";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(870, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(164, 20);
            this.label4.TabIndex = 28;
            this.label4.Text = "Spaces between lines";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Location = new System.Drawing.Point(21, 36);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1268, 238);
            this.tabControl1.TabIndex = 34;
            this.tabControl1.Visible = false;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.textBox3);
            this.tabPage6.Controls.Add(this.label25);
            this.tabPage6.Controls.Add(this.checkBox1);
            this.tabPage6.Controls.Add(this.button9);
            this.tabPage6.Controls.Add(this.label20);
            this.tabPage6.Controls.Add(this.label17);
            this.tabPage6.Controls.Add(this.label24);
            this.tabPage6.Controls.Add(this.label22);
            this.tabPage6.Controls.Add(this.textBox2);
            this.tabPage6.Controls.Add(this.label23);
            this.tabPage6.Controls.Add(this.radioButton1);
            this.tabPage6.Controls.Add(this.textBox1);
            this.tabPage6.Controls.Add(this.radioButton2);
            this.tabPage6.Controls.Add(this.label21);
            this.tabPage6.Location = new System.Drawing.Point(4, 29);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1260, 205);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Distance measurement ";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.label8);
            this.tabPage10.Controls.Add(this.label6);
            this.tabPage10.Controls.Add(this.sharpenTrackBar);
            this.tabPage10.Controls.Add(this.contrastTrackBar);
            this.tabPage10.Controls.Add(this.sharpenValue);
            this.tabPage10.Controls.Add(this.label7);
            this.tabPage10.Controls.Add(this.brightnessValue);
            this.tabPage10.Controls.Add(this.brightnessTrackBar);
            this.tabPage10.Controls.Add(this.contrastValue);
            this.tabPage10.Location = new System.Drawing.Point(4, 29);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(1260, 205);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "Adjustments";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.checkBox2);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.bgrGray);
            this.tabPage1.Controls.Add(this.resize);
            this.tabPage1.Controls.Add(this.button6);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.flipVerticaly);
            this.tabPage1.Controls.Add(this.widthSize);
            this.tabPage1.Controls.Add(this.flipHorizontally);
            this.tabPage1.Controls.Add(this.heightSize);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1260, 205);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Modification";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.numericUpDown1);
            this.tabPage2.Controls.Add(this.label27);
            this.tabPage2.Controls.Add(this.spaceSigma);
            this.tabPage2.Controls.Add(this.medianBtn);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.gaussianKernel);
            this.tabPage2.Controls.Add(this.colorSigma);
            this.tabPage2.Controls.Add(this.gaussianBtn);
            this.tabPage2.Controls.Add(this.bilatralBtn);
            this.tabPage2.Controls.Add(this.bilatralKernel);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1260, 205);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Smoothing";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(1043, 126);
            this.numericUpDown1.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(125, 26);
            this.numericUpDown1.TabIndex = 15;
            this.numericUpDown1.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(1023, 82);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(145, 20);
            this.label27.TabIndex = 14;
            this.label27.Text = "Aperture linear size";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label28);
            this.tabPage3.Controls.Add(this.blockSizeControl);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.binarizationValue);
            this.tabPage3.Controls.Add(this.trackBar1);
            this.tabPage3.Controls.Add(this.button7);
            this.tabPage3.Controls.Add(this.binarizationBtn);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1260, 205);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Binarization";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(467, 122);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(132, 20);
            this.label28.TabIndex = 18;
            this.label28.Text = "Block size control";
            // 
            // blockSizeControl
            // 
            this.blockSizeControl.Location = new System.Drawing.Point(625, 117);
            this.blockSizeControl.Name = "blockSizeControl";
            this.blockSizeControl.Size = new System.Drawing.Size(100, 26);
            this.blockSizeControl.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(463, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "Threshold control";
            // 
            // binarizationValue
            // 
            this.binarizationValue.AutoSize = true;
            this.binarizationValue.Location = new System.Drawing.Point(1139, 41);
            this.binarizationValue.Name = "binarizationValue";
            this.binarizationValue.Size = new System.Drawing.Size(18, 20);
            this.binarizationValue.TabIndex = 15;
            this.binarizationValue.Text = "0";
            // 
            // trackBar1
            // 
            this.trackBar1.BackColor = System.Drawing.SystemColors.Window;
            this.trackBar1.Cursor = System.Windows.Forms.Cursors.Default;
            this.trackBar1.Location = new System.Drawing.Point(625, 41);
            this.trackBar1.Maximum = 254;
            this.trackBar1.Minimum = 10;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(493, 69);
            this.trackBar1.TabIndex = 14;
            this.trackBar1.TickFrequency = 5;
            this.trackBar1.Value = 10;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.cleanGrains);
            this.tabPage4.Controls.Add(this.cannyBtn);
            this.tabPage4.Controls.Add(this.gbSizeUpDown);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.threshLinkingCanny);
            this.tabPage4.Controls.Add(this.button2);
            this.tabPage4.Controls.Add(this.threshCanny);
            this.tabPage4.Controls.Add(this.sobelBtn);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.laplacianBtn);
            this.tabPage4.Controls.Add(this.aperatureSize);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1260, 205);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Edge detection";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.skeletizationBtn);
            this.tabPage5.Controls.Add(this.blackHatMorph);
            this.tabPage5.Controls.Add(this.button1);
            this.tabPage5.Controls.Add(this.closeMorph);
            this.tabPage5.Controls.Add(this.label2);
            this.tabPage5.Controls.Add(this.topHatMorph);
            this.tabPage5.Controls.Add(this.openMorph);
            this.tabPage5.Controls.Add(this.button8);
            this.tabPage5.Controls.Add(this.numeric);
            this.tabPage5.Location = new System.Drawing.Point(4, 29);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1260, 205);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Morpgological operations";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.BorderExpansion);
            this.tabPage7.Controls.Add(this.getDualPhase);
            this.tabPage7.Controls.Add(this.BUM);
            this.tabPage7.Location = new System.Drawing.Point(4, 29);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(1260, 205);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Dual phase";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.label26);
            this.tabPage8.Controls.Add(this.button15);
            this.tabPage8.Controls.Add(this.button14);
            this.tabPage8.Controls.Add(this.pixelSurfaceArea);
            this.tabPage8.Controls.Add(this.button5);
            this.tabPage8.Controls.Add(this.button11);
            this.tabPage8.Controls.Add(this.button13);
            this.tabPage8.Controls.Add(this.button12);
            this.tabPage8.Controls.Add(this.textBox4);
            this.tabPage8.Controls.Add(this.label1);
            this.tabPage8.Location = new System.Drawing.Point(4, 29);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(1260, 205);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Statistic";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(279, 29);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(76, 20);
            this.label26.TabIndex = 45;
            this.label26.Text = "1px [ μm ]";
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(625, 26);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(112, 109);
            this.button15.TabIndex = 44;
            this.button15.Text = "Grain range";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click_1);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(484, 26);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(135, 109);
            this.button14.TabIndex = 41;
            this.button14.Text = "Grain diameter";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // pixelSurfaceArea
            // 
            this.pixelSurfaceArea.Location = new System.Drawing.Point(362, 26);
            this.pixelSurfaceArea.Name = "pixelSurfaceArea";
            this.pixelSurfaceArea.Size = new System.Drawing.Size(100, 26);
            this.pixelSurfaceArea.TabIndex = 30;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(261, 74);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(201, 61);
            this.button5.TabIndex = 29;
            this.button5.Text = "Grain Surface Area";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.button18);
            this.tabPage9.Controls.Add(this.button17);
            this.tabPage9.Controls.Add(this.button16);
            this.tabPage9.Controls.Add(this.button10);
            this.tabPage9.Controls.Add(this.button3);
            this.tabPage9.Controls.Add(this.button4);
            this.tabPage9.Controls.Add(this.horizontalLines);
            this.tabPage9.Controls.Add(this.getColors);
            this.tabPage9.Controls.Add(this.drawLines);
            this.tabPage9.Controls.Add(this.verticalLines);
            this.tabPage9.Controls.Add(this.numberOfLines);
            this.tabPage9.Controls.Add(this.spaceBetweenLines);
            this.tabPage9.Controls.Add(this.label4);
            this.tabPage9.Controls.Add(this.label3);
            this.tabPage9.Location = new System.Drawing.Point(4, 29);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(1260, 205);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "Export statistics";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(11, 27);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(108, 133);
            this.button17.TabIndex = 46;
            this.button17.Text = "Export grain range";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(145, 27);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(115, 133);
            this.button16.TabIndex = 45;
            this.button16.Text = "Export grain diameter";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(288, 25);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(118, 135);
            this.button10.TabIndex = 44;
            this.button10.Text = "Export grain surface area";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click_1);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(429, 23);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(129, 137);
            this.button3.TabIndex = 39;
            this.button3.Text = "Export %";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1133, 23);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(93, 127);
            this.button4.TabIndex = 40;
            this.button4.Text = "Export red points";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // horizontalLines
            // 
            this.horizontalLines.AutoSize = true;
            this.horizontalLines.Location = new System.Drawing.Point(758, 78);
            this.horizontalLines.Name = "horizontalLines";
            this.horizontalLines.Size = new System.Drawing.Size(106, 24);
            this.horizontalLines.TabIndex = 43;
            this.horizontalLines.TabStop = true;
            this.horizontalLines.Text = "Horizontal";
            this.horizontalLines.UseVisualStyleBackColor = true;
            // 
            // verticalLines
            // 
            this.verticalLines.AutoSize = true;
            this.verticalLines.Location = new System.Drawing.Point(773, 27);
            this.verticalLines.Name = "verticalLines";
            this.verticalLines.Size = new System.Drawing.Size(91, 24);
            this.verticalLines.TabIndex = 42;
            this.verticalLines.TabStop = true;
            this.verticalLines.Text = "Vertical ";
            this.verticalLines.UseVisualStyleBackColor = true;
            // 
            // applyImageSize
            // 
            this.applyImageSize.Location = new System.Drawing.Point(489, 1082);
            this.applyImageSize.Name = "applyImageSize";
            this.applyImageSize.Size = new System.Drawing.Size(180, 52);
            this.applyImageSize.TabIndex = 36;
            this.applyImageSize.Text = "Apply Image Size";
            this.applyImageSize.UseVisualStyleBackColor = true;
            this.applyImageSize.Click += new System.EventHandler(this.applyImageSize_Click);
            // 
            // trackBar1_horizontal
            // 
            this.trackBar1_horizontal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar1_horizontal.Location = new System.Drawing.Point(241, 276);
            this.trackBar1_horizontal.Maximum = 100;
            this.trackBar1_horizontal.Name = "trackBar1_horizontal";
            this.trackBar1_horizontal.Size = new System.Drawing.Size(797, 69);
            this.trackBar1_horizontal.TabIndex = 37;
            this.trackBar1_horizontal.Value = 100;
            this.trackBar1_horizontal.Scroll += new System.EventHandler(this.trackBar1_horizontal_Scroll);
            // 
            // trackBar1_vertical
            // 
            this.trackBar1_vertical.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.trackBar1_vertical.Location = new System.Drawing.Point(21, 299);
            this.trackBar1_vertical.Maximum = 100;
            this.trackBar1_vertical.Name = "trackBar1_vertical";
            this.trackBar1_vertical.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar1_vertical.Size = new System.Drawing.Size(69, 1017);
            this.trackBar1_vertical.TabIndex = 38;
            this.trackBar1_vertical.Scroll += new System.EventHandler(this.trackBar1_vertical_Scroll);
            // 
            // chart2
            // 
            chartArea3.AxisX.ScaleView.MinSizeType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea3.AxisX.ScaleView.SizeType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea3.Name = "ChartArea1";
            chartArea3.Position.Auto = false;
            chartArea3.Position.Height = 94F;
            chartArea3.Position.Width = 81.59F;
            chartArea3.Position.X = 3F;
            chartArea3.Position.Y = 3F;
            this.chart2.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart2.Legends.Add(legend3);
            this.chart2.Location = new System.Drawing.Point(1338, 52);
            this.chart2.Name = "chart2";
            series3.ChartArea = "ChartArea1";
            series3.IsXValueIndexed = true;
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            series3.XValueMember = "Keys";
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series3.YValueMembers = "Values";
            series3.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            this.chart2.Series.Add(series3);
            this.chart2.Size = new System.Drawing.Size(978, 216);
            this.chart2.TabIndex = 39;
            this.chart2.Text = "chart2";
            title3.Alignment = System.Drawing.ContentAlignment.TopRight;
            title3.Name = "Grain Size";
            this.chart2.Titles.Add(title3);
            this.chart2.Visible = false;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(596, 25);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(128, 135);
            this.button18.TabIndex = 47;
            this.button18.Text = "Export % dual phase";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2085, 1384);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.trackBar1_vertical);
            this.Controls.Add(this.trackBar1_horizontal);
            this.Controls.Add(this.applyImageSize);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aperatureSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threshLinkingCanny)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.threshCanny)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbSizeUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.widthSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sharpenTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contrastTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brightnessTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spaceBetweenLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spaceSigma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorSigma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bilatralKernel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gaussianKernel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1_horizontal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1_vertical)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openBMPToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown numeric;
        private System.Windows.Forms.ToolStripMenuItem saveImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bMPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tXTToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem cleanSpaceToolStripMenuItem;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.NumericUpDown gbSizeUpDown;
        private System.Windows.Forms.Button cannyBtn;
        private System.Windows.Forms.NumericUpDown threshCanny;
        private System.Windows.Forms.NumericUpDown threshLinkingCanny;
        private System.Windows.Forms.Button laplacianBtn;
        private System.Windows.Forms.NumericUpDown aperatureSize;
        private System.Windows.Forms.Button sobelBtn;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button binarizationBtn;
        private System.Windows.Forms.Button bgrGray;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown heightSize;
        private System.Windows.Forms.NumericUpDown widthSize;
        private System.Windows.Forms.Button flipVerticaly;
        private System.Windows.Forms.Button flipHorizontally;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label sharpenValue;
        private System.Windows.Forms.Label contrastValue;
        private System.Windows.Forms.Label brightnessValue;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TrackBar sharpenTrackBar;
        private System.Windows.Forms.TrackBar contrastTrackBar;
        private System.Windows.Forms.TrackBar brightnessTrackBar;
        private System.Windows.Forms.Button gaussianBtn;
        private System.Windows.Forms.Button bilatralBtn;
        private System.Windows.Forms.Button medianBtn;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown gaussianKernel;
        private System.Windows.Forms.NumericUpDown bilatralKernel;
        private System.Windows.Forms.Button resize;
        private System.Windows.Forms.NumericUpDown spaceSigma;
        private System.Windows.Forms.NumericUpDown colorSigma;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button skeletizationBtn;
        private System.Windows.Forms.Button cleanGrains;
        public System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button BUM;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button drawLines;
        private System.Windows.Forms.NumericUpDown spaceBetweenLines;
        private System.Windows.Forms.NumericUpDown numberOfLines;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button getDualPhase;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button closeMorph;
        private System.Windows.Forms.Button openMorph;
        private System.Windows.Forms.Button topHatMorph;
        private System.Windows.Forms.Button blackHatMorph;
        private System.Windows.Forms.Button BorderExpansion;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.CheckBox getColors;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Button applyImageSize;
        private System.Windows.Forms.TrackBar trackBar1_horizontal;
        private System.Windows.Forms.TrackBar trackBar1_vertical;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label binarizationValue;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox pixelSurfaceArea;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.RadioButton verticalLines;
        private System.Windows.Forms.RadioButton horizontalLines;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox blockSizeControl;
        private System.Windows.Forms.Button button18;
    }
}

